# Check for fd and bfs availability once
typeset -r HAS_FD=$(command -v fd >/dev/null 2>&1 && echo 1 || echo 0)
typeset -r HAS_BFS=$(command -v bfs >/dev/null 2>&1 && echo 1 || echo 0)

FZF_FD_EXCLUDES="--exclude=\"{.git,node_modules,site-packages,.mypy_cache,.ropeproject}/\""

if (( HAS_FD )); then
  FZF_DEFAULT_COMMAND='fd --threads="$(($(nproc) / 4))" --type f --no-ignore-vcs --hidden --strip-cwd-prefix '
  FZF_DEFAULT_COMMAND+="--follow $FZF_FD_EXCLUDES 2>/dev/null"
else
  FZF_DEFAULT_COMMAND='find . -type f -not -path "*/\.git/*" -not -path "*/node_modules/*" \
    -not -path "*/site-packages/*" -not -path "*/.mypy_cache/*" -not -path "*/.ropeproject/*"'
fi
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
FZF_PREVIEW_CMD="( $HOME/.scripts/preview {} || bat --style=plain --paging=never --color=always {} || cat {} || tree -C {} | head -n 200 )  2>/dev/null"

export FZF_CTRL_T_OPTS="--preview '${FZF_PREVIEW_CMD}'"

if (( HAS_BFS )); then
  export FZF_ALT_C_COMMAND="bfs -type d 2>/dev/null"
else
  export FZF_ALT_C_COMMAND="find . -type d -not -path '*/\.git/*' -not -path '*/node_modules/*' \
    -not -path '*/site-packages/*' -not -path '*/.mypy_cache/*' -not -path '*/.ropeproject/*'"
fi
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -n 200'"

# Options to fzf command
export FZF_COMPLETION_OPTS='--border --info=inline'

# Enable fzf_tmux only if TMUX version supports popups (3.2+)
if [[ -n "$TMUX" ]]; then
  local tmux_version=$(tmux -V | sed 's/[^0-9.]//g')
  if [[ "$(printf '%s\n' "3.2" "$tmux_version" | sort -V | head -n1)" == "3.2" ]]; then
    export FZF_TMUX=1
    export FZF_TMUX_OPTS="-p100%,60%"
  else
    export FZF_TMUX=0
  fi
fi

# Use fd (https://github.com/sharkdp/fd) for listing path candidates.
# - The first argument to the function ($1) is the base path to start traversal
# - See the source code (completion.{bash,zsh}) for the details.
_fzf_compgen_path() {
  if (( HAS_FD )); then
    fd --threads="$(($(nproc) / 4))" --hidden --follow --no-ignore-vcs $FZF_FD_EXCLUDES . "$1"
  else
    find "$1" -type f -not -path '*/\.git/*' -not -path '*/node_modules/*' \
      -not -path '*/site-packages/*' -not -path '*/.mypy_cache/*' -not -path '*/.ropeproject/*'
  fi
}

# Use fd/find to generate the list for directory completion
_fzf_compgen_dir() {
  if (( HAS_FD )); then
    fd --threads="$(($(nproc) / 4))" --type d --hidden --no-ignore-vcs --follow $FZF_FD_EXCLUDES . "$1"
  else
    find "$1" -type d -not -path '*/\.git/*' -not -path '*/node_modules/*' \
      -not -path '*/site-packages/*' -not -path '*/.mypy_cache/*' -not -path '*/.ropeproject/*'
  fi
}

# Advanced customization of fzf options via _fzf_comprun function
# - The first argument to the function is the name of the command.
# - You should make sure to pass the rest of the arguments to fzf.
_fzf_comprun() {
  local command=$1
  shift

  case "$command" in
    cd)           fzf --preview 'tree -C {} | head -200'   "$@" ;;
    export|unset) fzf --preview "eval 'echo \$'{}"         "$@" ;;
    ssh)          fzf --preview 'dig {}'                   "$@" ;;
    *)            fzf --preview "$FZF_PREVIEW_CMD"         "$@" ;;
  esac
}

# Source FZF key bindings and completion
for file in \
    /usr/share/fzf/key-bindings.zsh \
    /usr/share/doc/fzf/examples/key-bindings.zsh \
    /usr/share/fzf/shell/key-bindings.zsh \
    ; do
    [[ -f "$file" ]] && source "$file" && break
done

for file in /usr/share/fzf/completion.zsh \
    /usr/share/doc/fzf/examples/completion.zsh \
    /usr/share/fzf/shell/completion.zsh \
    ; do
    [[ -f "$file" ]] && source "$file" && break
done

