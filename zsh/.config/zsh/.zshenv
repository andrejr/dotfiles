setopt no_global_rcs
#
# Defines environment variables.
#

#
# ~/ Cleanup
#

# Definitions
export XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}"
export XDG_DATA_HOME="${XDG_DATA_HOME:-$HOME/.local/share}"
export XDG_CACHE_HOME="${XDG_CACHE_HOME:-$HOME/.cache}"

# Add Flatpak directories to XDG_DATA_DIRS if they exist
flatpak_dirs=(
    '/var/lib/flatpak/exports/share'
    "$HOME/.local/share/flatpak/exports/share"
    '/usr/local/share'
    '/usr/share'
)

for dir in "${flatpak_dirs[@]}"; do
    if [[ -d "$dir" ]]; then
        export XDG_DATA_DIRS="$dir:$XDG_DATA_DIRS"
    fi
done

# Aliases

alias info='info --init-file "$XDG_CONFIG_HOME/info/infokey"'
alias sqlite3='sqlite3 -init "$XDG_CONFIG_HOME"/sqlite3/sqliterc'
alias wget='wget --hsts-file="$XDG_CACHE_HOME/wget-hsts"'

# Dirs
# export ALSA_CONFIG_PATH="$XDG_CONFIG_HOME/alsa/asoundrc"
export CARGO_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/cargo"
export CCACHE_CONFIGPATH="$XDG_CONFIG_HOME"/ccache/ccache.config
export CCACHE_DIR="$XDG_CACHE_HOME"/ccache
export GEM_HOME="${XDG_DATA_HOME:-$HOME/.local/share}"/gem
export GEM_SPEC_CACHE="$XDG_CACHE_HOME"/gem
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"
export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"
export HISTFILE="$XDG_DATA_HOME"/zsh/zsh_history
export INPUTRC="${XDG_CONFIG_HOME:-$HOME/.config}/input/.inputrc"
export IPYTHONDIR="$XDG_CONFIG_HOME"/ipython
export KODI_DATA="${XDG_DATA_HOME:-$HOME/.local/share}/kodi"
# export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export OCTAVE_HISTFILE="$XDG_CACHE_HOME/octave-hsts"
export OCTAVE_SITE_INITFILE="$XDG_CONFIG_HOME/octave/octaverc"
export PYLINTHOME="$XDG_CACHE_HOME"/pylint
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export SCREENRC="$XDG_CONFIG_HOME"/screen/screenrc
export SQLITE_HISTORY=$XDG_DATA_HOME/sqlite_history
export TMUX_TMPDIR="$XDG_RUNTIME_DIR"
export WEECHAT_HOME="$XDG_CONFIG_HOME"/weechat
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export WINEPREFIX="${XDG_DATA_HOME:-$HOME/.local/share}/wineprefixes/default"
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export LESSHISTFILE=-
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
export ZDATADIR="${XDG_DATA_HOME}/zsh"

[ -d "$XDG_CACHE_HOME"/zsh ] || mkdir -p "$XDG_CACHE_HOME"/zsh

#
# Editors
#

# Set VISUAL and EDITOR to nvim, vim, or vi, whichever is available
if command -v nvim >/dev/null 2>&1; then
    export VISUAL="nvim"
    export EDITOR="nvim"
elif command -v vim >/dev/null 2>&1; then
    export VISUAL="vim"
    export EDITOR="vim"
else
    export VISUAL="vi"
    export EDITOR="vi"
fi
export PAGER='less'
export TERMINAL="alacritty"

export MANPATH=/usr/local/texlive/latest/texmf-dist/doc/man:$MANPATH
export INFOPATH=/usr/local/texlive/latest/texmf-dist/doc/info:$INFOPATH

export RIPGREP_CONFIG_PATH="$HOME/.config/rg/.ripgreprc"

path=(
    /usr/local/{bin,sbin}
    "/home/$(whoami)/.local/bin"
	"/home/$(whoami)/go/bin"
    "/home/$(whoami)/.dotnet/tools"
	"/usr/local/texlive/latest/bin/x86_64-linux"
	$path
)

# Dasht docsets directory
export DASHT_DOCSETS_DIR=/home/$(whoami)/.local/share/Zeal/Zeal/docsets

# Ensure that a non-login, non-interactive shell has a defined environment.
if [[ ($SHLVL -eq 1 && ! -o LOGIN) && -s "${ZDOTDIR:-$HOME}/.zprofile" ]]; then
	source "${ZDOTDIR:-$HOME}/.zprofile"
fi

#
# Development-related
#

# mise
command -v mise >/dev/null && eval "$(mise activate zsh)"

export MAKEFLAGS="-j$(nproc)"

export CPPUTEST_HOME="/home/$(whoami)/devel/libs/cpp/cpputest"

export ANDROID_NDK=/opt/android-ndk

# Set DOCKER_HOST to podman socket if it exists
PODMAN_SOCK_PATH="/var/run/user/$(id -u)/podman/podman.sock"
if [[ -S "$PODMAN_SOCK_PATH" ]]; then
    export DOCKER_HOST="unix://$PODMAN_SOCK_PATH"
fi

export ZK_PATH=~/zettelkasten
export ZK_STOPWORDS="$ZK_PATH/stopwords"

# API keys and such

# Source private environment variables if the file exists
if [[ -f "$ZDOTDIR/.zshenv_private" ]]; then
    source "$ZDOTDIR/.zshenv_private"
fi
