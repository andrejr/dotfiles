# Plugin definitions
declare -A ZSH_PLUGINS=(
  ["fast-syntax-highlighting"]="https://github.com/zdharma-continuum/fast-syntax-highlighting.git fast-syntax-highlighting.plugin.zsh"
  ["zsh-autosuggestions"]="https://github.com/zsh-users/zsh-autosuggestions.git zsh-autosuggestions.zsh"
  ["powerlevel10k"]="https://github.com/romkatv/powerlevel10k.git powerlevel10k.zsh-theme"
  ["zsh-history-substring-search"]="https://github.com/zsh-users/zsh-history-substring-search.git zsh-history-substring-search.zsh"
  ["zsh-completions"]="https://github.com/zsh-users/zsh-completions.git zsh-completions.plugin.zsh"
  ["zchee-zsh-completions"]="https://github.com/zchee/zsh-completions.git zsh-completions.plugin.zsh"
)

function zcompile-many() {
  local f
  for f; do zcompile -R -- "$f".zwc "$f"; done
}

function ensure_plugin() {
  local plugin_name="$1"
  local plugin_info="$2"
  local git_url="${plugin_info%% *}"
  local plugin_path="$ZDATADIR/plugins/$plugin_name"

  if [[ ! -e $plugin_path ]]; then
    git clone --depth=1 "$git_url" "$plugin_path"

    # Special handling for powerlevel10k
    if [[ $plugin_name == "powerlevel10k" ]]; then
      make -C "$plugin_path" pkg
    else
      # Compile plugin files
      local source_path="${plugin_info#* }"
      zcompile-many "$plugin_path/$source_path" "$plugin_path"/**/*.zsh(N)
    fi
  fi
}

function source_plugin() {
  local plugin_name="$1"
  local plugin_info="$2"
  local source_path="${plugin_info#* }"
  local plugin_dir="$ZDATADIR/plugins/$plugin_name"

  # Check last update time from central timestamp file
  if [[ -f "$ZDATADIR/plugins/.last_update" ]]; then
    local last_modified=$(stat -c %Y "$ZDATADIR/plugins/.last_update")
    local current_time=$(date +%s)
    local days_since_update=$(( (current_time - last_modified) / 86400 ))

    if (( days_since_update > 30 )); then
      echo "Warning: Plugins haven't been updated in $days_since_update days. Consider running 'zsh_update_plugins'" >&2
    fi
  fi

  source "$plugin_dir/$source_path"
}

function zsh_update_plugins() {
  local plugin_name
  for plugin_name in "${(k)ZSH_PLUGINS[@]}"; do
    echo "Updating plugin: $plugin_name"
    local plugin_path="$ZDATADIR/plugins/$plugin_name"
    ensure_plugin "$plugin_name" "${ZSH_PLUGINS[$plugin_name]}"
    (cd "$plugin_path" && git pull --ff-only)
    source_plugin "$plugin_name" "${ZSH_PLUGINS[$plugin_name]}"
  done
  touch "$ZDATADIR/plugins/.last_update"
  echo "All plugins updated and sourced."
}

# Ensure all plugins are installed
for plugin_name in "${(k)ZSH_PLUGINS[@]}"; do
  ensure_plugin "$plugin_name" "${ZSH_PLUGINS[$plugin_name]}"
done

# Install TPM (Tmux Plugin Manager) if not already installed
if [[ ! -e $XDG_DATA_HOME/tmux/plugins/tpm ]]; then
  git clone https://github.com/tmux-plugins/tpm $XDG_DATA_HOME/tmux/plugins/tpm
fi

# Ensure proper permissions for zsh directories and history
mkdir -p "${ZDATADIR}/plugins"
chmod 755 "${ZDATADIR}" "${ZDATADIR}/plugins" -R

# Load plugins
for plugin_name in "${(k)ZSH_PLUGINS[@]}"; do
  source_plugin "$plugin_name" "${ZSH_PLUGINS[$plugin_name]}"
done
