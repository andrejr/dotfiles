###############################################################################
#                              Early Initialization                           #
###############################################################################
# Start or attach to tmux if not already running
if [[ -z ${TMUX+X}${ZSH_SCRIPT+X}${ZSH_EXECUTION_STRING+X} ]]; then
  if [[ -n "$SSH_CLIENT" ]]; then
    # In SSH session: attach to existing session or create new one
    if tmux has-session 2>/dev/null; then
      exec tmux attach
    else
      exec tmux new-session
    fi
  else
    # Local session: just create new session
    exec tmux
  fi
fi

# Uncomment to enable profiling
# zmodload zsh/zprof

###############################################################################
#                              Plugin Management                              #
###############################################################################
# Load plugin management system
source "$ZDOTDIR/plugins.zsh"

# Enable Powerlevel10k Instant Prompt
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Load p10k configuration
[[ ! -f $ZDOTDIR/.p10k.zsh ]] || source $ZDOTDIR/.p10k.zsh

###############################################################################
#                            Completion System Setup                          #
###############################################################################
# Initialize completion system
COMP_DUMP="$ZDATADIR/zcompdump-$ZSH_VERSION"
autoload -Uz compinit -d "$COMP_DUMP" && compinit -d "$COMP_DUMP"
_comp_options+=(globdots)

###############################################################################
#                              Core Configuration                             #
###############################################################################
# Load core configuration files
source "$ZDOTDIR/keybinds.zsh"
source "$ZDOTDIR/completion.zsh"
source "$ZDOTDIR/aliases.zsh"
source "$ZDOTDIR/fzf.zsh"
source "$ZDOTDIR/git-auto-fetch.zsh"

# Command editing
autoload -Uz edit-command-line
zle -N edit-command-line

###############################################################################
#                              History Configuration                          #
###############################################################################
# Setup history directory
mkdir -p "${XDG_DATA_HOME:-$HOME/.local/share}/zsh/"
chmod 755 "${XDG_DATA_HOME:-$HOME/.local/share}/zsh"

# Configure history
export HISTFILE=${XDG_DATA_HOME:-$HOME/.local/share}/zsh/zhistfile
touch "$HISTFILE"
chmod 700 "$HISTFILE"
export HISTSIZE=30000
export SAVEHIST="$HISTSIZE"
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_FIND_NO_DUPS
setopt hist_ignore_dups
setopt inc_append_history

export HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE=1

###############################################################################
#                                Base Aliases                                 #
###############################################################################
# File operations
alias ls="ls -A --color"
alias vim="nvim"
alias ycd='pwd | xclip -in -selection clipboard'

# TMUX aliases
alias t='tmux'
alias tp='tmuxp load'
alias ta='tmux attach -t'
alias tad='tmux attach -d -t'
alias ts='tmux new-session -s'
alias tl='tmux list-sessions'
alias tksv='tmux kill-server'
alias tkss='tmux kill-session -t'

# Package management (paru)
alias pi='paru --sync'                         # install package
alias pu='paru --sync --refresh --sysupgrade'  # update packages
alias pug='paru --sync --refresh --sysupgrade --devel --timeupdate' # update w/ git packages
alias prp='paru --remove'                      # remove package
alias psp='paru --sync --search'               # search for package remotely
alias psl='paru --query --search'              # search for package locally
alias pil='paru --query --info'                # local package info
alias pir='paru --sync --info'                 # remote package info
alias pll='paru --query --list'                # list local package's files
alias plr='paru --files --list'                # list remote package's files
alias pfl='paru --query --owns'                # check which local package owns file
alias pfr='paru --files'                       # check which remote package owns file
alias plo='paru --query --deps --unrequired'   # list orphans

# URL encoding/decoding
alias urldecode='python3 -c "import sys, urllib.parse as ul; \
    [sys.stdout.write(ul.unquote_plus(l)) for l in sys.stdin]"'
alias urlencode='python3 -c "import sys, urllib.parse as ul; \
    [sys.stdout.write(ul.quote_plus(l)) for l in sys.stdin]"'

###############################################################################
#                              Utility Functions                              #
###############################################################################
# Gitignore API functions
function gi() { curl -sLw "\n" "https://www.toptal.com/developers/gitignore/api/$@" }
function gil() { curl -sLw "\n" "https://www.toptal.com/developers/gitignore/api/list" }

###############################################################################
#                              System Management                              #
###############################################################################
# Rehash commands
alias zrh="rehash"
alias rehash-all="pkill zsh --signal=USR1"
alias zrha="rehash-all"

# Config reload commands
alias zcr="rm -f $COMP_DUMP; compinit -d $COMP_DUMP"
alias zrc='zsh_reload_config'
alias zrca="pkill zsh --signal=USR2"

# Signal handlers
catch_signal_usr1() {
	trap catch_signal_usr1 USR1
	if [[ -o INTERACTIVE ]]; then
		rehash
	fi
}
trap catch_signal_usr1 USR1

catch_signal_usr2() {
	trap catch_signal_usr2 USR2
	if [[ -o INTERACTIVE ]]; then
		zsh_reload_config
		rm -f "$COMP_DUMP"
		compinit -d "$COMP_DUMP"
	fi
}
trap catch_signal_usr2 USR2

zsh_reload_config() {
	printf '\nExecuting a new shell instance.\n' 1>&2
	exec "${SHELL}"
}

###############################################################################
#                              SSH Configuration                              #
###############################################################################
# SSH agent setup (requires: systemctl --user enable --now ssh-agent.service)
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
for key in ~/.ssh/id_*(N); do
  if [[ -f "$key" && ! "$key" =~ \.pub$ ]]; then
    ssh-add "$key" 2>/dev/null
  fi
done

###############################################################################
#                           Environment Variables                             #
###############################################################################
export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring
