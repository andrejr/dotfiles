-- function os.capture(cmd)
--     local f = assert(io.popen(cmd, 'r'))
--     local s = assert(f:read('*a'))
--     f:close()
--     return s
-- end

-- function Is_rt()
--     local uname = os.capture('uname -r')
--     return (uname:find('rt', 1, true) ~= nil) or (uname:find('realtime', 1, true) ~= nil)
-- end

rule = {
    matches = {
        {
            { "device.name", "not-equals", "alsa_card.usb-BEHRINGER_UMC404HD_192k-00" },
            { "media.class", "equals", "Audio/Device" }
        },
    },
    apply_properties = {
        ["device.disabled"] = true
    },
}
table.insert(alsa_monitor.rules, rule)

rule2 = {
    matches = {
        {
            { "device.name", "equals", "alsa_card.usb-BEHRINGER_UMC404HD_192k-00" },
        },
    },
    apply_properties = {
        -- ["api.alsa.disable-batch"] = false,
        -- ["api.alsa.period_size"] = 6,
        ["api.alsa.headroom"] = 64,
    },
}
table.insert(alsa_monitor.rules, rule2)
