# Powershell theme
if (Get-Command oh-my-posh -ErrorAction SilentlyContinue)
{
    if ($IsLinux)
    {
        oh-my-posh init pwsh `
            --config "$home/.config/powershell/pure.omp.json" `
        | Invoke-Expression
    } elseif ($IsWindows)
    {
        oh-my-posh init pwsh `
            --config "$home\Documents\PowerShell\pure.omp.json" `
        | Invoke-Expression
    }
}

# Check PowerShell Version
$PSVersion = $PSVersionTable.PSVersion.Major

# Sets Vi Mode
if ($PSVersion -ge 5)
{
    Set-PSReadLineOption -EditMode Vi
}

# Enable history prediction
if ($PSVersion -ge 7)
{
    Set-PSReadlineOption -PredictionSource history
}

# Emacs-like shortcuts
Set-PSReadlineKeyHandler -Chord 'Ctrl+w' -Function BackwardKillWord
Set-PSReadlineKeyHandler -Chord 'Ctrl+u' -Function BackwardKillLine
Set-PSReadlineKeyHandler -Chord 'Ctrl+k' -Function KillLine
Set-PSReadlineKeyHandler -Chord 'Ctrl+a' -Function BeginningOfLine

if ($PSVersion -ge 7)
{
    Set-PSReadlineKeyHandler -Chord "Ctrl+e" -ScriptBlock {
        [Microsoft.PowerShell.PSConsoleReadLine]::AcceptSuggestion()
        [Microsoft.PowerShell.PSConsoleReadLine]::EndOfLine()
    }
} elseif ($PSVersion -ge 5)
{
    Set-PSReadlineKeyHandler -Chord "Ctrl+e" -Function EndOfLine
}

Set-PSReadlineKeyHandler -Chord 'Ctrl+p' -Function PreviousHistory
Set-PSReadlineKeyHandler -Chord 'Ctrl+n' -Function NextHistory

# This command seems version independent
Set-PSReadlineKeyHandler -Chord 'Ctrl+d' -Function ViExit

# Colors
if ($PSVersion -ge 5)
{
    Set-PSReadLineOption -Colors @{
        InlinePrediction=[System.ConsoleColor]::DarkCyan
        Comment=[System.ConsoleColor]::Green
        Operator=[System.ConsoleColor]::DarkMagenta
        Parameter=[System.ConsoleColor]::DarkBlue
        Error=[System.ConsoleColor]::DarkRed
    }
}


# Git completion
if ($PSVersion -ge 5)
{
    # installs with
    # PowerShellGet\Install-Module posh-git -Scope CurrentUser -Force
    # updates with
    # PowerShellGet\Update-Module posh-git
    if (Get-Module -ListAvailable -Name posh-git)
    {
        Import-Module posh-git
        $GitPromptSettings.EnableFileStatus = $false
    }
}

# Aliases
New-Alias -Name vi -Value nvim
