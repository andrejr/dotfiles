#!/usr/bin/env zsh

for d in `ls -d */`
do
    ( stow -D ${d%%/})
done
