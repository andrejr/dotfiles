-- load standard vis module, providing parts of the Lua API
require('vis')
require('plugins/filetype')
require('plugins/textobject-lexer')
require('plugins/number-inc-dec')
require('plugins/complete-word')
require('plugins/complete-filename')
require('plugins/digraph')

plugin_vis_open =require('plugins/vis-fzf-open/fzf-open')
-- Path to the fzf executable (default: "fzf")
plugin_vis_open.fzf_path = "fzf"
-- Arguments passed to fzf (defaul: "")
 plugin_vis_open.fzf_args = "-q '!.class '"

require('plugins/vis-ctags/ctags')
require('plugins/vis-backup/backup')
require('plugins/vis-commentary/vis-commentary')
require('plugins/vis-fzf-open/fzf-open')
require('plugins/vis-modelines/vis-modelines')
require('plugins/vis-surround/surround')
require('plugins/vis-textobject-surround/textobject-surround')

vis.events.subscribe(vis.events.INIT, function()
	vis:command('set theme seoul256')
	-- vis:command('set theme light-16')
	-- vis:command('set theme solarized')
end)


vis.events.subscribe(vis.events.WIN_OPEN, function(win)
	vis:command('set tabwidth 4')
	vis:command('set expandtab yes')
	vis:command('set autoindent yes')
	vis:command('set shell "/bin/zsh"')
	vis:command('set number')
	vis:command('set relativenumbers on')
	vis:command('set show-tabs')
	vis:command('set colorcolumn 79')
	vis:command('set cursorline')
end)
