-- PaperColor codes Copyright (c) 2016 Nguyen Nguyen <NLKNguyen@MSN.com>
local lexers = vis.lexers

local colors = {
	['foreground']       = '#d0d0d0',
	['background']       = '#444444',

	['red']              = '#df0000', -- Include/Exception
	['darkgreen']        = '#005f5f',
	['green']            = '#87af87', -- Boolean/Special
	['lightgreen']       = '#d7d787', -- Boolean/Special
	['yellow']           = '#d7d7af', -- Boolean/Special
	['hotorange']        = '#d7875f', -- String
	['ochre']            = '#d7af5f', -- String
	['olive']            = '#afaf5f', -- String
	['tangerine']        = '#ffaf87', -- String

	['blue']             = '#5fafaf', -- Keyword

	['pink']             = '#d7005f', -- Type
	['navy']             = '#005f87', -- StorageClass

	['orange']           = '#ffd787', -- Number
	['purple']           = '#8959a8', -- Repeat/Conditional
	['aqua']             = '#87afaf', -- Operator/Delimiter

	['magenta']          = '#d75f87',
	['violet']           = '#6c71c4',

	['cursor_bg']        = '#5faf87',
	['cursor_fg']        = '#3a3a3a',
	['cursorline']       = '#3a3a3a',
    ['selection']        = '#005f5f',
	['comment']          = '#5f875f',

	['status_fg']        = '#d7d7af',
	['status_uf_bg']     = '#585858',
	['status_f_bg']      = '#875f5f',

    ['line_nr_fg']       = '#87875f',
    ['line_nr_bg']       = '#4e4e4e',


}

lexers.colors = colors
-- light
local fg = ',fore:'..colors.foreground..','
local bg = ',back:'..colors.background..','

lexers.STYLE_DEFAULT        = bg..fg
lexers.STYLE_NOTHING        = bg
lexers.STYLE_CLASS          = 'fore:'..colors.navy
lexers.STYLE_COMMENT        = 'fore:'..colors.comment
lexers.STYLE_CONSTANT       = 'fore:'..colors.blue
lexers.STYLE_DEFINITION     = 'fore:'..colors.blue
lexers.STYLE_ERROR          = 'fore:'..colors.red..',italics'
lexers.STYLE_FUNCTION       = 'fore:'..colors.yellow
lexers.STYLE_KEYWORD        = 'fore:'..colors.magenta..',bold'
lexers.STYLE_LABEL          = 'fore:'..colors.green
lexers.STYLE_NUMBER         = 'fore:'..colors.orange
lexers.STYLE_OPERATOR       = 'fore:'..colors.lightgreen
lexers.STYLE_REGEX          = 'fore:'..colors.aqua
lexers.STYLE_STRING         = 'fore:'..colors.aqua
lexers.STYLE_PREPROCESSOR   = 'fore:'..colors.hotorange
lexers.STYLE_TAG            = 'fore:'..colors.tangerine
lexers.STYLE_TYPE           = 'fore:'..colors.ochre..',bold'
lexers.STYLE_VARIABLE       = 'fore:'..colors.aqua
lexers.STYLE_WHITESPACE     = ''
lexers.STYLE_EMBEDDED       = 'fore:'..colors.blue
lexers.STYLE_IDENTIFIER     = fg

lexers.STYLE_LINENUMBER     = 'fore:'..colors.line_nr_fg..',back:'..colors.line_nr_bg
lexers.STYLE_CURSOR         = 'fore:'..colors.cursor_fg..',back:'..colors.cursor_bg
lexers.STYLE_CURSOR_PRIMARY = lexers.STYLE_CURSOR..',back:'..colors.magenta
lexers.STYLE_CURSOR_LINE    = 'back:'..colors.cursorline
lexers.STYLE_COLOR_COLUMN   = 'back:'..colors.cursorline
lexers.STYLE_SELECTION      = 'back:'..colors.selection
lexers.STYLE_STATUS         = 'fore:'..colors.status_fg..',back:'..colors.status_uf_bg
lexers.STYLE_STATUS_FOCUSED = 'fore:'..colors.status_fg..',back:'..colors.status_f_bg..',bold'
lexers.STYLE_SEPARATOR      = lexers.STYLE_DEFAULT
lexers.STYLE_INFO           = 'fore:default,back:default,bold'
lexers.STYLE_EOF            = ''

