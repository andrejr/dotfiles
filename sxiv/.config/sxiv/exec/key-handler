#!/bin/sh

rotate() {
	file="$1"
	degree="$2"
	case "$(file -b -i "$file")" in
		image/jpeg*) jpegtran -rotate "$degree" -copy all -outfile "$file" "$file" ;;
		*) mogrify -rotate "$degree" "$file" ;;
	esac
}

flip() {
	file="$1"
	case "$(file -b -i "$file")" in
		image/jpeg*) jpegtran -flip horizontal -copy all -outfile "$file" "$file" ;;
		*) mogrify -flop "$file" ;;
	esac
}

HELP_TEXT=
read -r -d '' HELP_TEXT <<EOM
r - rotate clockwise
R - rotate counterclockwise
f - flip
e - show EXIF data
p - copy image path to clipboard
P - copy image path to clipboard, resolving symlinks
y - copy image into clipboard
Y - copy image file into clipboard
n - copy file name into clipboard
N - copy file name into clipboard, resolving symlinks
d - delete file
g - open file in Gimp
EOM

while read -r file; do
	case "$1" in
		"h")
			notify-send -a "sxiv" "$HELP_TEXT"
			;;
		"r")
			rotate "$file" 90
			;;
		"R")
			rotate "$file" -90
			;;
		"f")
			flip "$file"
			;;

		"e") alacritty \
			--title "$(basename "$file")" \
			--command sh \
			-c "exiv2 pr -q -pa \"$file\" | less" & ;;
		"p")
			printf "%s" "$file" | xclip -selection clipboard \
				&& notify-send -a "sxiv" "$file path copied to clipboard" &
			;;
		"P")
			printf "%s" "$(readlink -f "$file")" | xclip -selection clipboard \
				&& notify-send -a "sxiv" \
					"$(readlink -f "$file") path copied to clipboard" &
			;;

		"y")
			mimet=$(file -b --mime-type "$file") \
				&& xclip -selection clipboard -t "${mimet}" "$file" \
				&& notify-send -a "sxiv" "$file image copied to clipboard" \
				|| notify-send -a "sxiv" \
					--category=transfer.error \
					--urgency=critical \
					--icon=gtk-info \
					"Error copying image" &
			;;

		"Y")
			printf "%s" "$file" \
				| xclip -i -selection clipboard \
					-t x-special/gnome-copied-files \
				&& notify-send -a "sxiv" "$file file copied to clipboard" &
			;;
		"n")
			printf "%s" "$(basename "$file")" \
				| xclip -selection clipboard \
				&& notify-send -a \
					"sxiv" "$(basename "$file") name copied to clipboard" &
			;;
		"N")
			printf "%s" "$(basename "$(readlink -f "$file")")" \
				| xclip -selection clipboard \
				&& notify-send -a "sxiv" \
					"$(basename "$(readlink -f "$file")") name copied to clipboard" &
			;;
		"d")
			[ "$(printf 'No\nYes' | rofi -dmenu -i -p "Delete $file?")" = "Yes" ] \
				&& rm "$file" && notify-send -a "sxiv" "$file deleted."
			;;
		"g") ifinstalled gimp && gimp "$file" & ;;
	esac
done
