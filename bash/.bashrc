#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

shopt -s histappend
PROMPT_COMMAND="history -a;$PROMPT_COMMAND"
export HISTSIZE=30000
export HISTFILESIZE=30000

alias ls='ls --color=auto'

alias urldecode='python3 -c "import sys, urllib.parse as ul; \
    [sys.stdout.write(ul.unquote_plus(l)) for l in sys.stdin]"'

alias urlencode='python3 -c "import sys, urllib.parse as ul; \
    [sys.stdout.write(ul.quote_plus(l)) for l in sys.stdin]"'

export PS1="\[\033[38;5;12m\][\[$(tput sgr0)\]\[\033[38;5;10m\]\u\[$(tput sgr0)\]\[\033[38;5;12m\]@\[$(tput sgr0)\]\[\033[38;5;7m\]\h\[$(tput sgr0)\]\[\033[38;5;12m\]]\[$(tput sgr0)\]\[\033[38;5;15m\]: \[$(tput sgr0)\]\[\033[38;5;7m\]\w\[$(tput sgr0)\]\[\033[38;5;12m\]>\[$(tput sgr0)\]\[\033[38;5;10m\]\\$\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
