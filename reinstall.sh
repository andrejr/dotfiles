#!/usr/bin/env zsh

for d in `ls -d */`
do
    ( stow -R ${d%%/} --adopt)
done
