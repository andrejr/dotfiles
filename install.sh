#!/usr/bin/env zsh

for d in $(ls -d */)
do
    ( stow ${d%%/})
done
