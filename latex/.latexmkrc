# use lualatex
$pdf_previewer = 'start zathura';

$pdf_mode = 4;
$postscript_mode = $dvi_mode = 0;

$lualatex = "lualatex -interaction=nonstopmode -halt-on-error -file-line-error %O %P";

# additional files to clean.
$clean_ext = 'bbl glo gls hd loa run.xml thm xdv ind idx';
