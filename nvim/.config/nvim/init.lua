-- https://github.com/frans-johansson/lazy-nvim-starter/tree/main

package.path = vim.fn.stdpath("config") .. "/?.lua;" .. package.path
-- vim.opt.runtimepath:append(vim.fn.stdpath("config"))

-- vim.cmd('source ~/.config/nvim/init_old.vim')

-- require("nonplug_functions")
-- require("general")
-- require("nonplug_keymaps")
-- require("packages")

-- Handle plugins with lazy.nvim
require("core.lazy")

-- Other options
require("core.options")

-- Functions
require("core.functions")

-- Autocmd
require("core.autocmd")

-- General Neovim keymaps
require("core.keymaps")

-- Per-lang serttings
require("core.language_settings")
