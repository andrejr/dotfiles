return {
    {
        "ibhagwan/fzf-lua",
        -- optional for icon support
        dependencies = { "nvim-tree/nvim-web-devicons" },
        config = function ()
            -- calling `setup` is optional for customization
            local f = require("fzf-lua")
            f.setup({
                "max-perf",
                grep = {
                    rg_opts =
                    [[--column --line-number --no-heading --color=never --follow --hidden --smart-case --max-columns=4096 -g "!.git" -e]],
                }
            })

            -- Normal mode mappings
            vim.keymap.set("n", "<Leader><Leader>", function ()
                f.files()
            end, { silent = true })
            vim.keymap.set("n", "<Leader><Enter>", function ()
                f.buffers()
            end, { silent = true })
            vim.keymap.set("n", "<Leader>`", function ()
                f.marks()
            end, { silent = true })
            vim.keymap.set("n", "<Leader>:", function ()
                f.command_history()
            end, { silent = true })
            vim.keymap.set("n", "<Leader>/", function ()
                f.search_history()
            end, { silent = true })
            vim.keymap.set("n", "<Leader>h", function ()
                f.oldfiles()
            end, { silent = true })
            vim.keymap.set("n", "<Leader>t", function ()
                f.tags()
            end, { silent = true })
            vim.keymap.set("n", "<Leader>ag", function ()
                f.grep_cword()
            end, { silent = true })
            vim.keymap.set("x", "<Leader>ag", function ()
                f.grep_visual()
            end, { silent = true })
            vim.keymap.set("n", "<Leader>AG", function ()
                f.grep_cWORD()
            end, { silent = true })
            vim.keymap.set("n", "<Leader><tab>", function ()
                f.keymaps()
            end, { silent = true })

            -- Custom commands
            vim.api.nvim_create_user_command('Rg',
                function (opts)
                    f.grep({ search = opts.args, no_esc=true })
                end,
                { nargs = "*", desc = "Search current project with [rip]grep" }
            )

            -- Insert mode completions
            vim.keymap.set({ "i" }, "<C-x><C-f>", function ()
                f.complete_path()
            end, { silent = true, desc = "Fuzzy complete path" })
            vim.keymap.set({ "i" }, "<C-x><C-j>", function ()
                f.complete_file()
            end, {
                silent = true,
                desc = "Fuzzy complete file path",
            })
            vim.keymap.set({ "i" }, "<C-x><C-l>", function ()
                f.complete_line()
            end, { silent = true, desc = "Fuzzy complete line" })
        end,
    },
}
