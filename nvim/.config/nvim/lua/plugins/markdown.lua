-- https://github.com/aspeddro/pandoc.nvim
--
--
-- Pandoc {
--        Plug 'vim-pandoc/vim-pandoc'
--        Plug 'vim-pandoc/vim-pandoc-syntax'
--        Plug 'vim-pandoc/vim-rmarkdown', { 'for': 'rmd' }
--        Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }
-- }
--
--    " Pandoc {
--        let g:pandoc#formatting#mode = 'haA'
--        let g:pandoc#syntax#codeblocks#embeds#langs = [
--                    \'c',
--                    \'python',
--                    \'bash=sh',
--                    \'sh',
--                    \'cpp',
--                    \'make',
--                    \'tex',
--                    \'latex=tex',
--                    \'sql',
--                    \'rst',
--                    \]
--    " }
return {
    {
        "iamcco/markdown-preview.nvim",
        lazy = true,
        build = function ()
            vim.fn["mkdp#util#install"]()
        end,
        ft = 'markdown',
        config = function ()
            vim.g.mkdp_browser = "firefox"
            vim.g.mkdp_filetypes = { "markdown", "pandoc" }
        end,
    },
    "dhruvasagar/vim-table-mode",
    "Konfekt/vim-sentence-chopper",
}
