--- @param ns_id integer Get highlight groups for namespace ns_id
---              `nvim_get_namespaces()`. Use 0 to get global highlight groups
---              `:highlight`.
--- @param name string Name of highlight group to invert
local function invert(ns_id, name)
    local initial = vim.api.nvim_get_hl(ns_id, { name = name })
    vim.api.nvim_set_hl(ns_id, name, {
        fg = initial.fg,
        bg = initial.bg,
        cterm = initial.cterm,
        ctermfg = initial.cterm.ctermfg,
        ctermbg = initial.cterm.ctermbg,
        reverse = true,
        bold = true,
        force = true
    })
end

return {
    -- Colorschemes
    {
        "junegunn/seoul256.vim",
        lazy = false,    -- make sure we load this during startup if it is your main colorscheme
        priority = 1000, -- make sure to load this before all the other start plugins
        config = function ()
            -- load the colorscheme here
            vim.g.seoul256_background = 238
            vim.g.seoul256_srgb = 0
            vim.cmd([[colorscheme seoul256]])
            -- StatusLine     xxx cterm=reverse ctermfg=95 ctermbg=187 guifg=#9a7372 guibg=#dfdebd
            invert(0, 'StatusLine')
            invert(0, 'StatusLineNC')
            -- local slhl = vim.api.nvim_get_hl(0, { name = 'StatusLine' })
            -- vim.api.nvim_set_hl(0, 'StatusLine', {
            --     fg = slhl.fg,
            --     bg = slhl.bg,
            --     ctermfg = slhl.cterm.ctermfg,
            --     ctermbg = slhl.cterm.ctermbg
            -- })
        end,
    },
    {
        "NLKNguyen/papercolor-theme",
        lazy = true,
    },
    -- Colorize color codes (hex, rgb, terminal)
    {
        "catgoose/nvim-colorizer.lua",
        event = "BufReadPre",
        opts = { -- set to setup table
        },
    },
    -- Rainbow parentheses
    -- {
    --     "HiPhish/rainbow-delimiters.nvim",
    --     config = function()
    --         local rainbow_delimiters = require("rainbow-delimiters")

    --         vim.g.rainbow_delimiters = {
    --             strategy = {
    --                 [""] = rainbow_delimiters.strategy["global"],
    --                 vim = rainbow_delimiters.strategy["local"],
    --             },
    --             query = {
    --                 [""] = "rainbow-delimiters",
    --                 lua = "rainbow-blocks",
    --             },
    --             highlight = {
    --                 "RainbowDelimiterRed",
    --                 "RainbowDelimiterYellow",
    --                 "RainbowDelimiterBlue",
    --                 "RainbowDelimiterOrange",
    --                 "RainbowDelimiterGreen",
    --                 "RainbowDelimiterViolet",
    --                 "RainbowDelimiterCyan",
    --             },
    --         }
    --     end,
    -- },
}
