-- LSP Configuration & Plugins
-- see DAP from here:
-- https://github.com/williamboman/mason.nvim/blob/main/doc/mason.txt#L153
return {
    {
        "neovim/nvim-lspconfig",
        dependencies = {
            "williamboman/mason.nvim",
            "williamboman/mason-lspconfig.nvim",
            {
                "jay-babu/mason-null-ls.nvim",
                dependencies = { "nvim-lua/plenary.nvim" },
            },
            {
                "jay-babu/mason-nvim-dap.nvim",
                dependencies = { "mfussenegger/nvim-dap" },
            },
            {
                "rcarriga/nvim-dap-ui",
                dependencies = {
                    "mfussenegger/nvim-dap",
                    "nvim-neotest/nvim-nio"
                }
            },
            {
                "nvim-neotest/neotest",
                dependencies = {
                    "Issafalcon/neotest-dotnet",
                }
            },
            "nvimtools/none-ls.nvim",
            {
                "j-hui/fidget.nvim",
                event = "LspAttach",
            },
            -- Neovim setup for init.lua and plugin development with full
            -- signature help, docs and completion for the nvim lua API
            {
                "folke/lazydev.nvim",
                ft = "lua", -- only load on lua files
                opts = {
                    library = {
                        -- See the configuration section for more details
                        -- Load luvit types when the `vim.uv` word is found
                        "lazy.nvim",
                        "nvim-dap-ui",
                        { path = "luvit-meta/library", words = { "vim%.uv" } },
                    },
                },
            },
            { "Bilal2453/luvit-meta", lazy = true }, -- optional `vim.uv` typings
            {                                        -- optional completion source for require statements and module annotations
                "hrsh7th/nvim-cmp",
                opts = function (_, opts)
                    opts.sources = opts.sources or {}
                    table.insert(opts.sources, {
                        name = "lazydev",
                        group_index = 0, -- set group index to 0 to skip loading LuaLS completions
                    })
                end,
            },
            "RRethy/vim-illuminate",
            "hrsh7th/cmp-nvim-lsp",
            "b0o/schemastore.nvim",
            "Hoffs/omnisharp-extended-lsp.nvim"
        },
        config = function ()
            -- Set up Mason before anything else
            require('mason').setup({ PATH = "append" })

            -- Check for required executables
            local has_npm = vim.fn.executable('npm') == 1
            local has_dotnet = vim.fn.executable('dotnet') == 1

            -- Prepare LSP server list
            local lsp_servers = {
                "ansiblels",
                "ast_grep",
                "basedpyright",
                "bashls",
                "clangd",
                "cssls",
                "docker_compose_language_service",
                "dockerls",
                "emmet_language_server",
                "html",
                "jsonls",
                "lemminx",
                "lua_ls",
                "neocmake",
                "omnisharp",
                "pkgbuild_language_server",
                "powershell_es",
                "regal",
                "ruff",
                "sqlls",
                "stylelint_lsp",
                "taplo",
                "texlab",
                "ts_ls",
                "vimls",
                "yamlls",
            }

            -- Add conditional servers
            if has_npm then
                table.insert(lsp_servers, "awk_ls")
            end

            require("mason-lspconfig").setup({
                ensure_installed = lsp_servers,
                automatic_installation = { exclude = { "serve_d" } },
            })

            local dap = require("dap")

            require("mason-nvim-dap").setup({
                ensure_installed = {
                    "bash",
                    "chrome",
                    "codelldb",
                    "coreclr",
                    "cppdbg",
                    "firefox",
                    "python",
                },
                automatic_installation = true,
                handlers = {
                    function (config)
                        require("mason-nvim-dap").default_setup(config)
                    end,
                    coreclr = function (config)
                        config.adapters = {
                            type = "executable",
                            command = "netcoredbg",
                            args = { "--interpreter=vscode" },
                        }
                        dap.configurations.cs = {
                            {
                                type = "coreclr",
                                name = "launch - netcoredbg",
                                request = "launch",
                                program = function ()
                                    return vim.fn.input({
                                        prompt = "Path to dll: ",
                                        completion = "file",
                                    })
                                end,
                                -- console = "integratedTerminal",
                                -- stopAtEntry = false,
                                -- internalConsoleOptions = "openOnSessionStart",
                                justMyCode = false,
                                logging = {
                                    engineLogging = true,
                                },
                            },
                            {
                                type = "coreclr",
                                name = "attach - netcoredbg",
                                request = "attach",
                                processId = require("dap.utils").pick_process,
                                args = {},
                                stopAtEntry = false,
                                justMyCode = false,
                                logging = {
                                    engineLogging = true,
                                },
                            },
                        }
                        require("mason-nvim-dap").default_setup(config)
                    end,
                    python = function (config)
                        local function find_python_path()
                            -- First, check for VIRTUAL_ENV environment variable
                            local virtual_env = os.getenv('VIRTUAL_ENV')
                            if virtual_env then
                                return virtual_env .. '/bin/python'
                            end
                            -- If VIRTUAL_ENV is not set, fall back to checking for local virtual environments
                            local cwd = vim.fn.getcwd()
                            if vim.fn.executable(cwd .. '/venv/bin/python') == 1 then
                                return cwd .. '/venv/bin/python'
                            elseif vim.fn.executable(cwd .. '/.venv/bin/python') == 1 then
                                return cwd .. '/.venv/bin/python'
                            else
                                return '/usr/bin/python'
                            end
                        end

                        local masonpath = vim.fn.stdpath('data') .. '/mason'

                        config.adapters.python = {
                            type = "executable",
                            command = masonpath ..
                                '/packages/debugpy/venv/bin/python3',
                            args = {
                                "-m",
                                "debugpy.adapter",
                            },
                        }
                        config.adapters.netcoredbg = {
                            type = 'executable',
                            command = 'netcoredbg',
                            args = { '--interpreter=vscode' }
                        }
                        config.adapters.coreclr = {
                            type = 'executable',
                            command = 'netcoredbg',
                            args = { '--interpreter=vscode' }
                        }
                        dap.configurations.python = {
                            {
                                -- The first three options are required by nvim-dap
                                type = 'python'; -- the type here established the link to the adapter definition: `dap.adapters.python`
                                request = 'launch';
                                name =
                                "Python: launch current file (just my code)";
                                cwd = function ()
                                    return vim.fs.root(vim.fn.getcwd(),
                                        {
                                            'pyproject.toml'
                                        })
                                end,
                                program = "${file}"; -- This configuration will launch the current file if used.
                                pythonPath = find_python_path();
                            },
                            {
                                -- The first three options are required by nvim-dap
                                type = 'python'; -- the type here established the link to the adapter definition: `dap.adapters.python`
                                request = 'launch';
                                name =
                                "Python: launch current file (not just my code)";
                                justMyCode = false,
                                cwd = function ()
                                    return vim.fs.root(vim.fn.getcwd(),
                                        {
                                            'pyproject.toml'
                                        })
                                end,
                                program = "${file}"; -- This configuration will launch the current file if used.
                                pythonPath = find_python_path();
                            },
                            {
                                type = 'python';
                                request = 'launch';
                                name = 'Pytest: Current File';
                                module = 'pytest';
                                args = { '${file}' };
                                console = 'integratedTerminal',
                                cwd = function ()
                                    return vim.fs.root(vim.fn.getcwd(),
                                        {
                                            'pyproject.toml'
                                        })
                                end,
                                pythonPath = find_python_path();
                            },
                            {
                                type = 'python';
                                request = 'launch';
                                name = 'Pytest: All Tests';
                                module = 'pytest';
                                args = { '.' };
                                cwd = function ()
                                    return vim.fs.root(vim.fn.getcwd(),
                                        {
                                            'pyproject.toml'
                                        })
                                end,
                                pythonPath = find_python_path();
                            },
                        }
                        require('mason-nvim-dap').default_setup(config) -- don't forget this!
                    end,
                },
            })

            require("neotest").setup({
                adapters = {
                    require("neotest-dotnet")({
                        dap = {
                            -- Extra arguments for nvim-dap configuration
                            -- See https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for values
                            args = { justMyCode = false },
                            -- Enter the name of your dap adapter, the default value is netcoredbg
                            adapter_name = "coreclr"
                        },
                        -- Let the test-discovery know about your custom attributes (otherwise tests will not be picked up)
                        -- Note: Only custom attributes for non-parameterized tests should be added here. See the support note about parameterized tests
                        -- custom_attributes = {
                        --     xunit = { "MyCustomFactAttribute" },
                        --     nunit = { "MyCustomTestAttribute" },
                        --     mstest = { "MyCustomTestMethodAttribute" }
                        -- },
                        -- Provide any additional "dotnet test" CLI commands here. These will be applied to ALL test runs performed via neotest. These need to be a table of strings, ideally with one key-value pair per item.
                        dotnet_additional_args = {
                            "--verbosity detailed"
                        },
                        -- Tell neotest-dotnet to use either solution (requires .sln file) or project (requires .csproj or .fsproj file) as project root
                        -- Note: If neovim is opened from the solution root, using the 'project' setting may sometimes find all nested projects, however,
                        --       to locate all test projects in the solution more reliably (if a .sln file is present) then 'solution' is better.
                        discovery_root = "project" -- Default
                    })
                }
            })


            vim.keymap.set('n', '<Leader>vc',
                function () require('dap').continue() end)
            vim.keymap.set('n', '<Leader>vv',
                function () require('dap').step_over() end)
            vim.keymap.set('n', '<Leader>vi',
                function () require('dap').step_into() end)
            vim.keymap.set('n', '<Leader>vo',
                function () require('dap').step_out() end)
            vim.keymap.set('n', '<Leader>vb',
                function () require('dap').toggle_breakpoint() end)
            vim.keymap.set('n', '<Leader>vB',
                function () require('dap').set_breakpoint() end)
            vim.keymap.set('n', '<Leader>vl',
                function ()
                    require('dap').set_breakpoint(nil, nil,
                        vim.fn.input('Log point message: '))
                end)
            vim.keymap.set('n', '<Leader>vr',
                function () require('dap').repl.open() end)
            vim.keymap.set('n', '<Leader>vq',
                function () require('dap').run_last() end)
            vim.keymap.set({ 'n', 'v' }, '<Leader>vh', function ()
                require('dap.ui.widgets').hover()
            end)
            vim.keymap.set({ 'n', 'v' }, '<Leader>vp', function ()
                require('dap.ui.widgets').preview()
            end)
            vim.keymap.set('n', '<Leader>vf', function ()
                local widgets = require('dap.ui.widgets')
                widgets.centered_float(widgets.frames)
            end)
            vim.keymap.set('n', '<Leader>vs', function ()
                local widgets = require('dap.ui.widgets')
                widgets.centered_float(widgets.scopes)
            end)

            -- Quick access via keymap
            require("helpers.keys").map(
                "n",
                "<leader>M",
                "<cmd>Mason<cr>",
                "Show Mason"
            )

            -- Turn on LSP status information
            require("fidget").setup()

            -- Set up cool signs for diagnostics
            local signs = {
                Error = " ",
                Warn = " ",
                Hint = " ",
                Info = " ",
            }
            for type, icon in pairs(signs) do
                local hl = "DiagnosticSign" .. type
                vim.fn.sign_define(
                    hl,
                    { text = icon, texthl = hl, numhl = "" }
                )
            end

            -- Diagnostic config
            vim.diagnostic.config({
                virtual_text = false,
                signs = {
                    active = signs,
                },
                update_in_insert = true,
                underline = true,
                severity_sort = true,
                float = {
                    style = "minimal",
                    border = "rounded",
                    source = true,
                    header = "",
                    prefix = "",
                },
            })

            vim.keymap.set("n", "<space>Q", vim.diagnostic.open_float)
            vim.keymap.set("n", "<space>q", vim.diagnostic.setqflist)
            vim.keymap.set("n", "<space>l", vim.diagnostic.setloclist)

            -- Use LspAttach autocommand to only map the following keys
            -- after the language server attaches to the current buffer
            vim.api.nvim_create_autocmd("LspAttach", {
                group = vim.api.nvim_create_augroup("UserLspConfig", {}),
                callback = function (ev)
                    -- Enable completion triggered by <c-x><c-o>
                    vim.bo[ev.buf].omnifunc = "v:lua.vim.lsp.omnifunc"

                    -- Buffer local mappings.
                    -- See `:help vim.lsp.*` for documentation on any of the below functions
                    local opts = { buffer = ev.buf }
                    vim.keymap.set("n", "gD", vim.lsp.buf.declaration, opts)
                    vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
                    vim.keymap.set(
                        "n",
                        "gy",
                        vim.lsp.buf.type_definition,
                        opts
                    )
                    vim.keymap.set("n", "gr", vim.lsp.buf.references, opts)
                    vim.keymap.set("n", "K", vim.lsp.buf.hover, opts)
                    vim.keymap.set("n", "gI", vim.lsp.buf.implementation, opts)
                    vim.keymap.set(
                        "n",
                        "<C-k>",
                        vim.lsp.buf.signature_help,
                        opts
                    )
                    vim.keymap.set("n", "<space>rn", vim.lsp.buf.rename, opts)
                    vim.keymap.set(
                        { "n", "v" },
                        "<space>ca",
                        vim.lsp.buf.code_action,
                        opts
                    )
                    -- vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, opts)
                    -- vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, opts)
                    -- vim.keymap.set('n', '<space>wl', function()
                    --   print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
                    -- end, opts)
                    vim.keymap.set("n", "<space>f", function ()
                        vim.lsp.buf.format({
                            async = true,
                            filter = function (client)
                                return client.name ~= "ts_ls" and
                                    client.name ~= "omnisharp"
                            end,
                        })
                    end, opts)
                end,
            })

            -- nvim-cmp supports additional completion capabilities, so broadcast that to servers
            local capabilities = vim.lsp.protocol.make_client_capabilities()
            capabilities =
                require("cmp_nvim_lsp").default_capabilities(capabilities)
            capabilities.textDocument.completion.completionItem.snippetSupport =
                true

            -- Setup null-ls for non-ls tools
            -- Prepare null-ls tools list
            local null_ls_tools = {
                "gersemi",
                "prettier",
                "yamlfix",
            }

            -- Add conditional tools
            if has_dotnet then
                table.insert(null_ls_tools, "csharpier")
            end

            require("mason-null-ls").setup({
                ensure_installed = null_ls_tools,
                automatic_installation = true
            })

            -- null-ls
            local null_ls = require("null-ls")

            null_ls.setup({
                sources = {
                    -- null_ls.builtins.formatting.stylua,
                    null_ls.builtins.formatting.clang_format,
                    null_ls.builtins.formatting.csharpier,
                    null_ls.builtins.formatting.gersemi,
                    null_ls.builtins.formatting.prettier,
                    null_ls.builtins.formatting.shfmt,
                    null_ls.builtins.formatting.yamlfix.with({
                        env = {
                            YAMLFIX_WHITELINES = "1",
                            YAMLFIX_SECTION_WHITELINES = "1",
                            YAMLFIX_SEQUENCE_STYLE = "block_style",
                        },
                    }),
                },
            })

            -- Lua
            require("lspconfig")["lua_ls"].setup({
                capabilities = capabilities,
                settings = {
                    Lua = {
                        completion = {
                            callSnippet = "Replace",
                        },
                        diagnostics = {
                            globals = { "vim" },
                        },
                        workspace = {
                            library = {
                                [vim.fn.expand("$VIMRUNTIME/lua")] = true,
                                [vim.fn.stdpath("config") .. "/lua"] = true,
                            },
                        },
                    },
                },
            })

            -- Python
            require('lspconfig').ruff.setup({
                trace = 'messages',
                init_options = {
                    settings = {
                        logLevel = 'debug',
                    }
                }
            })
            require('lspconfig').basedpyright.setup {
                settings = {
                    basedpyright = {
                        -- Using Ruff's import organizer
                        disableOrganizeImports = true,
                        -- analysis = {
                        --     -- Ignore all files for analysis to exclusively use Ruff for linting
                        --     ignore = { '*' },
                        -- },
                        typeCheckingMode = "standard",
                    },
                },
            }
            -- Disable Ruff hover in favor of Pyright
            vim.api.nvim_create_autocmd("LspAttach", {
                group = vim.api.nvim_create_augroup(
                    'lsp_attach_disable_ruff_hover', {
                        clear = true
                    }),
                callback = function (args)
                    local client = vim.lsp.get_client_by_id(args.data.client_id)
                    if client == nil then
                        return
                    end
                    if client.name == 'ruff' then
                        -- Disable hover in favor of Pyright
                        client.server_capabilities.hoverProvider = false
                    end
                end,
                desc = 'LSP: Disable hover capability from Ruff',
            })

            -- Ansible
            require("lspconfig")["ansiblels"].setup({})

            -- ast-grep
            require('lspconfig')["ast_grep"].setup({})

            -- Run detect_ansible for each .yaml file
            local ansible_au = vim.api.nvim_create_augroup("ansible", {
                clear = true,
            })
            vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead" }, {
                group = ansible_au,
                callback = function ()
                    vim.opt_local.ft = "yaml.ansible"
                end,
                pattern = {
                    "*/playbooks/*.yml",
                    "*/playbooks/*.yaml",
                    "*/roles/*/tasks/*.yml",
                    "*/roles/*/tasks/*.yaml",
                    "*/roles/*/handlers/*.yml",
                    "*/roles/*/handlers/*.yaml",
                },
            })

            vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead" }, {
                group = ansible_au,
                callback = function ()
                    -- Get the current file's directory
                    local dir = vim.fn.expand("%:p:h")

                    -- Form the potential path to the ansible.cfg
                    local ansible_cfg_path = dir .. "/ansible.cfg"

                    -- Check if ansible.cfg exists in the directory
                    if vim.fn.filereadable(ansible_cfg_path) == 1 then
                        vim.bo.filetype = "yaml.ansible"
                    end
                end,
                pattern = {
                    "*.yml",
                    "*.yaml",
                },
            })

            -- docker-compose
            require("lspconfig")["docker_compose_language_service"].setup({
                filetypes = { "yaml.docker-compose", "docker-compose" },
            })

            -- yaml
            require("lspconfig")["yamlls"].setup({
                filetypes = {
                    "yaml",
                    "yaml.docker-compose",
                    "yaml.ansible",
                },
                settings = {
                    yaml = {
                        schemaStore = {
                            -- You must disable built-in schemaStore support if you want to use
                            -- this plugin and its advanced options like `ignore`.
                            enable = false,
                            -- Avoid TypeError: Cannot read properties of undefined (reading 'length')
                            url = "",
                        },
                        schemas = require("schemastore").yaml.schemas({
                            -- Catalog here:
                            -- https://github.com/SchemaStore/schemastore/blob/master/src/api/json/catalog.json
                            ignore = {
                                "Deployer Recipe",
                            },
                        }),
                    },
                    redhat = {
                        telemetry = {
                            enabled = false,
                        },
                    },
                },
            })

            -- Emmet
            require("lspconfig")["emmet_language_server"].setup({
                capabilities = capabilities,
                filetypes = {
                    "css",
                    "eruby",
                    "html",
                    "javascript",
                    "javascriptreact",
                    "less",
                    "sass",
                    "scss",
                    "svelte",
                    "pug",
                    "typescriptreact",
                    "vue",
                    "jsx",
                },
                init_options = {
                    html = {
                        options = {
                            ["bem.enabled"] = true,
                        },
                    },
                },
            })

            -- Omnisharp
            require("lspconfig")["omnisharp"].setup({
                capabilities = capabilities,
                enable_roslyn_analyzers = true,
                handlers = {
                    ["textDocument/definition"] = require('omnisharp_extended')
                        .handler,
                },
                cmd = {
                    "omnisharp",
                    '--languageserver',
                    '--hostPID',
                    tostring(vim.fn.getpid())
                },
            })

            require("lspconfig").serve_d.setup({ cmd = { "serve-d" } })


            if has_npm then
                require("lspconfig")["awk_ls"].setup({})
            end
            require("lspconfig")["bashls"].setup({})
            require("lspconfig")["clangd"].setup({
                on_attach = function (_, bufnr)
                    local function buf_set_keymap(...)
                        vim.api
                            .nvim_buf_set_keymap(bufnr, ...)
                    end
                    local opts = { noremap = true, silent = true }
                    buf_set_keymap('n', '<leader>o',
                        '<Cmd>ClangdSwitchSourceHeader<CR>', opts)
                end
            })
            require("lspconfig")["dockerls"].setup({})
            require("lspconfig")["html"].setup({})
            require("lspconfig")["jsonls"].setup({})
            require("lspconfig")["neocmake"].setup({
                capabilities = capabilities,
                filetypes = { "cmake" },
                root_dir = function (fname)
                    return require("lspconfig").util.find_git_ancestor(fname)
                end,
                single_file_support = true, -- suggested
                init_options = {
                    format = {
                        enable = false
                    },
                    lint = {
                        enable = true
                    },
                    scan_cmake_in_package = true -- default is true
                }

            })
            require("lspconfig")["lemminx"].setup({})
            require("lspconfig")["pkgbuild_language_server"].setup({})
            require("lspconfig")["powershell_es"].setup({})
            require("lspconfig")["regal"].setup({})
            require("lspconfig")["sqlls"].setup({})
            require("lspconfig")["stylelint_lsp"].setup({})
            require("lspconfig")["taplo"].setup({})
            require("lspconfig")["texlab"].setup({})
            require("lspconfig")["ts_ls"].setup({})
            require("lspconfig")["vimls"].setup({})


            local dapui = require("dapui")
            dapui.setup()
            dap.listeners.before.attach.dapui_config = function ()
                dapui.open()
            end
            dap.listeners.before.launch.dapui_config = function ()
                dapui.open()
            end
            dap.listeners.before.event_terminated.dapui_config = function ()
                dapui.close()
            end
            dap.listeners.before.event_exited.dapui_config = function ()
                dapui.close()
            end
        end,
    },
    {
        "rachartier/tiny-inline-diagnostic.nvim",
        event = "VeryLazy", -- Or `LspAttach`
        config = function ()
            require('tiny-inline-diagnostic').setup({
                signs = {
                    left = " ",
                    right = "",
                    diag = " •",
                    arrow = "    ",
                    up_arrow = " ↑   ",
                    vertical = " │",
                    vertical_end = " └",
                },
                options = {
                    -- Show the source of the diagnostic.
                    show_source = true,
                    multiple_diag_under_cursor = true,
                }
            })
        end
    }
}
