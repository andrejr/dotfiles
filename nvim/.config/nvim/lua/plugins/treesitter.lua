-- Highlight, edit, and navigate code
return {
    {
        "nvim-treesitter/nvim-treesitter",
        build = function ()
            pcall(
                require("nvim-treesitter.install").update({ with_sync = true })
            )
        end,
        event = { "BufReadPost", "BufNewFile" },
        dependencies = {
            "nvim-treesitter/nvim-treesitter-textobjects",
            "nvim-treesitter/nvim-tree-docs",
            "JoosepAlviste/nvim-ts-context-commentstring",
            "nvim-treesitter/playground",
        },
        config = function ()
            require("nvim-treesitter.configs").setup({
                -- Add languages to be installed here that you want installed for treesitter
                ensure_installed = {
                    "arduino",
                    "bash",
                    "bibtex",
                    "c",
                    "c_sharp",
                    "cmake",
                    "cpp",
                    "css",
                    "d",
                    "diff",
                    "dockerfile",
                    "dot",
                    "git_config",
                    "git_rebase",
                    "gitattributes",
                    "gitcommit",
                    "gitignore",
                    "go",
                    "graphql",
                    "hjson",
                    "html",
                    "http",
                    "ini",
                    "java",
                    "javascript",
                    "jq",
                    "json",
                    "json5",
                    "jsonc",
                    "latex",
                    "lua",
                    "luadoc",
                    "luap",
                    "luau",
                    "make",
                    "markdown",
                    "markdown_inline",
                    "nix",
                    "passwd",
                    "python",
                    "query",
                    "promql",
                    "regex",
                    "rego",
                    "rst",
                    "rust",
                    "sql",
                    "thrift",
                    "toml",
                    "typescript",
                    "vim",
                    "vimdoc",
                    "yaml",
                },
                ignore_install = { "help" },

                ts_context_commentstring = {
                    enable = true,
                    enable_autocmd = false,
                },

                highlight = { enable = true },
                indent = { enable = true, disable = { "python" } },
                incremental_selection = {
                    enable = true,
                    keymaps = {
                        init_selection = "<c-space>",
                        node_incremental = "<c-space>",
                        scope_incremental = "<c-s>",
                        node_decremental = "<c-backspace>",
                    },
                },
                textobjects = {
                    select = {
                        enable = true,
                        lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
                        keymaps = {
                            -- You can use the capture groups defined in textobjects.scm
                            ["aa"] = "@parameter.outer",
                            ["ia"] = "@parameter.inner",
                            ["af"] = "@function.outer",
                            ["if"] = "@function.inner",
                            ["ac"] = "@class.outer",
                            ["ic"] = "@class.inner",
                            ["as"] = {
                                query = "@scope",
                                query_group = "locals",
                                desc = "Select language scope",
                            },
                        },
                    },
                    -- https://github.com/sho-87/dotfiles/blob/master/nvim/lua/plugins/modules/treesitter.lua
                    move = {
                        enable = true,
                        set_jumps = true, -- whether to set jumps in the jumplist
                        goto_next_start = {
                            ["]m"] = "@function.outer",
                            ["]]"] = "@class.outer",
                        },
                        goto_next_end = {
                            ["]M"] = "@function.outer",
                            ["]["] = "@class.outer",
                        },
                        goto_previous_start = {
                            ["[m"] = "@function.outer",
                            ["[["] = "@class.outer",
                        },
                        goto_previous_end = {
                            ["[M"] = "@function.outer",
                            ["[]"] = "@class.outer",
                        },
                    },
                    swap = {
                        enable = true,
                        swap_next = {
                            ["<leader>z"] = "@parameter.inner",
                        },
                        swap_previous = {
                            ["<leader>Z"] = "@parameter.inner",
                        },
                    },
                },
                tree_docs = {
                    enable = true,
                    keymaps = {
                        doc_all_in_range = "<Leader>x",
                        doc_node_at_cursor = "<Leader>x",
                        edit_doc_at_cursor = "<Leader>xe",
                    },
                },
                playground = {
                    enable = true,
                    disable = {},
                    updatetime = 25,         -- Debounced time for highlighting nodes in the playground from source code
                    persist_queries = false, -- Whether the query persists across vim sessions
                    keybindings = {
                        toggle_query_editor = "o",
                        toggle_hl_groups = "i",
                        toggle_injected_languages = "t",
                        toggle_anonymous_nodes = "a",
                        toggle_language_display = "I",
                        focus_language = "f",
                        unfocus_language = "F",
                        update = "R",
                        goto_node = "<cr>",
                        show_help = "?",
                    },
                },
            })
            vim.api.nvim_create_user_command("SyntaxHere", function ()
                local pos = vim.api.nvim_win_get_cursor(0)
                local line, col = pos[1], pos[2] + 1
                print("Regex (legacy) Syntax Highlights")
                print("--------------------------------")
                print(
                    " effective: "
                    .. vim.fn.synIDattr(
                        vim.fn.synID(line, col, true),
                        "name"
                    )
                )
                for _, synId in ipairs(vim.fn.synstack(line, col)) do
                    local synGroupId = vim.fn.synIDtrans(synId)
                    print(
                        " "
                        .. vim.fn.synIDattr(synId, "name")
                        .. " -> "
                        .. vim.fn.synIDattr(synGroupId, "name")
                    )
                end
                print(" ")
                print("Tree-sitter Syntax Highlights")
                print("--------------------------------")
                vim.print(vim.treesitter.get_captures_at_cursor(0))
                print(" ")
                print("Vim's :Inspect")
                print("--------------------------------")
                vim.cmd([[:Inspect]])
            end, {})
        end,
    },
    {
        "Wansmer/treesj",
        keys = { "<space>m", "<space>j", "<space>s" },
        dependencies = { "nvim-treesitter/nvim-treesitter" },
        opts = {},
    },
    -- https://github.com/mimmanuel/nvim-treesitter-powershell
}
