return {
    {
        "chrisgrieser/nvim-various-textobjs",
        opts = { useDefaultKeymaps = false },
        config = function ()
            local keymap = vim.keymap.set
            keymap(
                { "o", "x" },
                "ii",
                "<cmd>lua require('various-textobjs').indentation(true, true)<CR>"
            )
            keymap(
                { "o", "x" },
                "ai",
                "<cmd>lua require('various-textobjs').indentation(false, true)<CR>"
            )
            keymap(
                { "o", "x" },
                "iI",
                "<cmd>lua require('various-textobjs').indentation(true, true)<CR>"
            )
            keymap(
                { "o", "x" },
                "aI",
                "<cmd>lua require('various-textobjs').indentation(false, false)<CR>"
            )

            keymap(
                { "o", "x" },
                "iv",
                "<cmd>lua require('various-textobjs').subword(true)<CR>"
            )
            keymap(
                { "o", "x" },
                "av",
                "<cmd>lua require('various-textobjs').subword(false)<CR>"
            )

            keymap(
                { "o", "x" },
                "iG",
                "<cmd>lua require('various-textobjs').entireBuffer()<CR>"
            )

            keymap(
                { "o", "x" },
                "il",
                "<cmd>lua require('various-textobjs').lineCharacterwise(true)<CR>"
            )
            keymap(
                { "o", "x" },
                "al",
                "<cmd>lua require('various-textobjs').lineCharacterwise(false)<CR>"
            )

            -- keymap(
            --     { "o", "x" },
            --     "YOUR_MAPPING",
            --     "<cmd>lua require('various-textobjs').value(true)<CR>"
            -- )
            -- keymap(
            --     { "o", "x" },
            --     "YOUR_MAPPING",
            --     "<cmd>lua require('various-textobjs').value(false)<CR>"
            -- )

            -- keymap(
            --     { "o", "x" },
            --     "YOUR_MAPPING",
            --     "<cmd>lua require('various-textobjs').key(true)<CR>"
            -- )
            -- keymap(
            --     { "o", "x" },
            --     "YOUR_MAPPING",
            --     "<cmd>lua require('various-textobjs').key(false)<CR>"
            -- )

            -- keymap(
            --     { "o", "x" },
            --     "YOUR_MAPPING",
            --     "<cmd>lua require('various-textobjs').diagnostic()<CR>"
            -- )

            -- keymap(
            --     { "o", "x" },
            --     "YOUR_MAPPING",
            --     "<cmd>lua require('various-textobjs').closedFold(true)<CR>"
            -- )
            -- keymap(
            --     { "o", "x" },
            --     "YOUR_MAPPING",
            --     "<cmd>lua require('various-textobjs').closedFold(false)<CR>"
            -- )

            -- keymap(
            --     { "o", "x" },
            --     "YOUR_MAPPING",
            --     "<cmd>lua require('various-textobjs').chainMember(true)<CR>"
            -- )
            -- keymap(
            --     { "o", "x" },
            --     "YOUR_MAPPING",
            --     "<cmd>lua require('various-textobjs').chainMember(false)<CR>"
            -- )

            -- keymap(
            --     { "o", "x" },
            --     "YOUR_MAPPING",
            --     "<cmd>lua require('various-textobjs').visibleInWindow()<CR>"
            -- )
            -- keymap(
            --     { "o", "x" },
            --     "YOUR_MAPPING",
            --     "<cmd>lua require('various-textobjs').restOfWindow()<CR>"
            -- )
        end,
    },
    "tpope/vim-surround", -- Surround stuff with the ys-, cs-, ds- commands

    --        Plug 'wellle/targets.vim'
    --        Plug 'kana/vim-textobj-user'
    --        Plug 'kana/vim-textobj-function'
    --        Plug 'glts/vim-textobj-comment'
    --        Plug 'thinca/vim-textobj-function-javascript'
    --        Plug 'Julian/vim-textobj-variable-segment'
}
