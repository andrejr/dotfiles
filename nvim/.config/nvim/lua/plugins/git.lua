-- Git related plugins
return {
    {
        "lewis6991/gitsigns.nvim",
        opts = {
            on_attach = function (bufnr)
                local gs = package.loaded.gitsigns

                local function map(mode, l, r, opts)
                    opts = opts or {}
                    opts.buffer = bufnr
                    vim.keymap.set(mode, l, r, opts)
                end

                -- Navigation
                map("n", "]c", function ()
                    if vim.wo.diff then
                        return "]c"
                    end
                    vim.schedule(function ()
                        gs.next_hunk()
                    end)
                    return "<Ignore>"
                end, { expr = true })

                map("n", "[c", function ()
                    if vim.wo.diff then
                        return "[c"
                    end
                    vim.schedule(function ()
                        gs.prev_hunk()
                    end)
                    return "<Ignore>"
                end, { expr = true })

                -- Actions
                -- map("n", "<leader>hs", gs.stage_hunk)
                -- map("n", "<leader>hr", gs.reset_hunk)
                -- map("v", "<leader>hs", function ()
                --     gs.stage_hunk({ vim.fn.line("."), vim.fn.line("v") })
                -- end)
                -- map("v", "<leader>hr", function ()
                --     gs.reset_hunk({ vim.fn.line("."), vim.fn.line("v") })
                -- end)
                -- map("n", "<leader>hS", gs.stage_buffer)
                -- map("n", "<leader>hu", gs.undo_stage_hunk)
                -- map("n", "<leader>hR", gs.reset_buffer)
                -- map("n", "<leader>hp", gs.preview_hunk)
                -- map("n", "<leader>hb", function ()
                --     gs.blame_line({ full = true })
                -- end)
                -- map("n", "<leader>tb", gs.toggle_current_line_blame)
                -- map("n", "<leader>hd", gs.diffthis)
                -- map("n", "<leader>hD", function ()
                --     gs.diffthis("~")
                -- end)
                -- map("n", "<leader>td", gs.toggle_deleted)
                --
                -- -- Text object
                -- map({ "o", "x" }, "ih", ":<C-U>Gitsigns select_hunk<CR>")
            end,
        },
    },
    {
        "tpope/vim-fugitive",
        dependencies = {
            "tpope/vim-rhubarb",
            "shumphrey/fugitive-gitlab.vim",
            "tpope/vim-git",
        },
        config = function ()
            local map = require("helpers.keys").map
            map(
                "n",
                "<leader>ga",
                "<cmd>Git add %<cr>",
                "Stage the current file"
            )
            map("n", "<leader>gb", "<cmd>Git blame<cr>", "Show the blame")

            vim.cmd([[
            nnoremap <silent> <leader>gs :Git<CR>
            nnoremap <silent> <leader>gd :Gdiff<CR>
            nnoremap <silent> <leader>gc :Gcommit<CR>
            nnoremap <silent> <leader>gb :Git blame<CR>
            nnoremap <silent> <leader>gl :Gclog<CR>
            nnoremap <silent> <leader>gp :Git push<CR>
            nnoremap <silent> <leader>gr :Gread<CR>
            nnoremap <silent> <leader>gw :Gwrite<CR>
            nnoremap <silent> <leader>ge :Gedit<CR>
            nnoremap <silent> <leader>gh :GBrowse<CR>
            xnoremap <silent> <leader>gh :'<,'>GBrowse<CR>
            " Mnemonic _i_nteractive
            nnoremap <silent> <leader>gi :Git add -p %<CR>
            ]])
            -- For :GBrowse
            vim.g.fugitive_gitlab_domains = { "https://git.symmetry.host" }
            vim.api.nvim_create_user_command(
                'Browse',
                function (opts)
                    vim.fn.setreg('+', opts.fargs[1])
                    vim.fn.system { 'xdg-open', opts.fargs[1] }
                end,
                { nargs = 1 }
            )
        end,
    },
    {
        "rbong/vim-flog",
        config = function ()
            vim.keymap.set("n", "<leader>gv", "<cmd>Flog<CR>")
        end,
    },
}
