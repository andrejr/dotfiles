return {
    {
        -- alternative 'tpope/vim-commentary',
        {
            "numToStr/Comment.nvim",
            dependencies = {
                "JoosepAlviste/nvim-ts-context-commentstring",
            },
            keys = {
                { "gc", mode = { "n", "v" }, "gcc" },
                { "gb", mode = { "n", "v" } },
            },
            lazy = true,
            config = function ()
                require("Comment").setup({
                    pre_hook = require(
                        "ts_context_commentstring.integrations.comment_nvim"
                    ).create_pre_hook(),
                })
                local ft = require("Comment.ft")
                ft({ "d", "go", "rust" }, ft.get("c"))
            end,
        },
    },
}
