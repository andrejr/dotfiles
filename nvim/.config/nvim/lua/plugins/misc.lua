-- Miscelaneous fun stuff
return {
    -- Better buffer closing actions. Available via the buffers helper.
    {
        "kazhala/close-buffers.nvim",
        opts = {
            preserve_window_layout = { "this", "nameless" },
        },
    },
    -- Editorconfig support
    "gpanders/editorconfig.nvim",
    {                  -- Manipulating surrounding braces
        "kylechui/nvim-surround",
        version = "*", -- Use for stability; omit to use `main` branch for the latest features
        event = "VeryLazy",
        config = function ()
            require("nvim-surround").setup({
                -- Configuration here, or leave empty to use defaults
            })
        end,
    },

    -- Repeating custom actions
    "tpope/vim-repeat",

    -- Delete / move / rename files while editing them
    -- alternative: "chrisgrieser/nvim-genghis",
    "tpope/vim-eunuch",

    -- Asynchronous make
    "tpope/vim-dispatch",

    { -- Session management
        'tpope/vim-obsession',
        lazy = true,
        cmd = 'Obsession'
    },

    -- Handy bracket mappings
    "tpope/vim-unimpaired",

    -- Undo visualization
    -- alternative: debugloop/telescope-undo.nvim
    "mbbill/undotree",

    -- Showing currently open buffers in cmdline
    "bling/vim-bufferline",
    -- Aligning text/code on characters
    -- alternative: https://github.com/echasnovski/mini.nvim/blob/main/readmes/mini-align.md
    {
        "junegunn/vim-easy-align",
        config = function ()
            vim.keymap.set({ "n", "x" }, "ga", "<Plug>(EasyAlign)", {
                desc = "Easy-align",
            })
        end,
    },
    -- Clear search marks upon motion, visual star search
    {
        "https://gitlab.com/andrejr/vim-slash-lite.git",
        config = function ()
            vim.cmd([[
                        nnoremap <silent> <Esc> :noh<CR><Esc>
                    ]])
            vim.g.slash_lite_avoid_mapping_gd = 1
        end,
    },
    -- Move through CamelCase and snake_case 'sentences' with motions
    {
        "chrisgrieser/nvim-spider",
        config = function ()
            vim.keymap.set({ "n", "o", "x" }, "<space>w", function ()
                require("spider").motion("w")
            end, {
                desc = "Spider-w",
            })
            vim.keymap.set({ "n", "o", "x" }, "<space>e", function ()
                require("spider").motion("e")
            end, {
                desc = "Spider-e",
            })
            vim.keymap.set({ "n", "o", "x" }, "<space>b", function ()
                require("spider").motion("b")
            end, {
                desc = "Spider-b",
            })
            vim.keymap.set({ "n", "o", "x" }, "<space>ge", function ()
                require("spider").motion("ge")
            end, {
                desc = "Spider-ge",
            })
        end,
    },
}
