return {
    -- Number conversions
    "glts/vim-radical",
    "glts/vim-magnum",
    -- Substitution of word variants; CamelCase, snake_case, etc. conversion
    "tpope/vim-abolish",
}
