-- Functions implementation {

-- Note command {
--  Adds a new note to your zettelkasten (opens it for editing)
vim.cmd [[
    function! s:NoteCommand(...) abort
        let zk_path = $ZK_PATH
        let title = trim(join(a:000))
        let path = zk_path . '/'
                    \ . strftime("%Y%m%d%H%M")
                    \ . " "
                    \ . title . ".md"

        execute ":e " . fnameescape(path)

        let newfile = ! filereadable(expand(path))
        if newfile
            call append(0, '# ' . title)
        endif

    endfunction

    command! -nargs=* Note call s:NoteCommand(<f-args>)
]]
-- }

-- ProjectorMode {
    -- Running :ProjectorMode changes to a white, high-contrast theme,
    -- convenient for doing work on a projector
vim.cmd [[
function! s:ProjectorMode()
    Change colors
    set background=light
    colorscheme PaperColor

    Remove Rainbow
    if exists(':RainbowParentheses!')
        execute ('RainbowParentheses!')
    endif

    Large font for nvim_qt
    if (g:GuiLoaded==1)
        Guifont Hack:h11
    endif

endfunction
:com! ProjectorMode :call s:ProjectorMode()
]]
-- }

-- Strip trailing whitespace {
vim.cmd [[
function! s:StripTrailingWhitespace(first_line, last_line)
    " Special precutions taken to avoid side effects (moving cursor,
    " finding history changes)
    " http://vi.stackexchange.com/questions/5949/substitute-with-pure-vimscript-without-s
    " %smagic/\s\+$//e
    " is replaced with iterating over lines in file, using substitute
    " on them and putting them back with setline
    let l:line_num = a:first_line
    for l:line in getline(a:first_line, a:last_line)
        call setline(l:line_num, substitute(l:line, '\s\+$', '', 'e'))
        let l:line_num += 1
    endfor
endfunction
command! -range=% StripTrailingWhitespace
            \ :call s:StripTrailingWhitespace(<line1>, <line2>)
]]
-- }

-- Edit / load config {

    -- vim.keymap.set('', 'vimrc', 'edit and reload keys')
    -- let s:edit_config_mapping = '<Leader>ve'
    -- let s:apply_config_mapping = '<Leader>vl'

    -- function! s:ExpandFilenameAndExecute(command, file)
        -- execute a:command . ' ' . expand(a:file, ':p')
    -- endfunction

    -- function! s:EditConfig()
        -- if isdirectory(expand($HOME . '/dotfiles/nvim/.config/nvim/'))
            -- call <SID>ExpandFilenameAndExecute('tabedit',
                        -- \ '~/dotfiles/nvim/.config/nvim/init.vim')
            -- call <SID>ExpandFilenameAndExecute('vsplit',
                        -- \ '~/dotfiles/nvim/.config/nvim/bundles.vim')
        -- else
            -- call <SID>ExpandFilenameAndExecute('tabedit',
                        -- \ '~/.config/nvim/init.vim')
            -- call <SID>ExpandFilenameAndExecute('vsplit',
                        -- \ '~/.config/nvim/bundles.vim')
        -- endif
        -- execute bufwinnr('bundles.vim') . 'wincmd w'
    -- endfunction

    -- execute 'noremap ' . s:edit_config_mapping .
                -- \ ' :call <SID>EditConfig()<CR>'
    -- if has('nvim')
        -- execute 'noremap ' . s:apply_config_mapping .
                    -- \ ' :source ~/.config/nvim/init.vim<CR>'
    -- else
        -- execute 'noremap ' . s:apply_config_mapping .
                    -- \ ' :source ~/.vimrc<CR>'
    -- endif
-- }

-- coc.nvim statusline function {
    -- if isdirectory(expand(g:plug_home . '/coc.nvim/'))
        -- function! StatusDiagnostic() abort
          -- let l:info = get(b:, 'coc_diagnostic_info', {})
          -- if empty(l:info) | return '' | endif
          -- let l:msgs = []
          -- if get(l:info, 'error', 0)
            -- call add(l:msgs, 'E' . l:info['error'])
          -- endif
          -- if get(l:info, 'warning', 0)
            -- call add(l:msgs, 'W' . l:info['warning'])
          -- endif
          -- let l:smart_status = get(g:, 'coc_status', '')
          -- if l:smart_status =~? '^\s*Python'
              -- let l:smart_status = ''
          -- endif
          -- return join(l:msgs, ' ') . ' ' . l:smart_status
        -- endfunction
    -- endif
-- }

