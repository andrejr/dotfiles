-- vim: set sw=4 ts=4 sts=4 et tw=78 foldmethod=marker spell: syntax=lua

-- environment {{{
if not vim.fn.has("win32") then
    vim.opt.shell = "/bin/bash"
end
-- }}}

-- local vimrc loading {{{

-- Loading local [n]vimrc files for projects
vim.opt.exrc = true
-- securely
vim.opt.secure = true
-- }}}

-- modelines {{{
-- Scan for vim modeline on open
vim.opt.modeline = true
-- Search the first 5
vim.opt.modelines = 5
-- }}}

-- dictionaries {{{
local dictionary_load_if_exists = function (path)
    if vim.loop.fs_stat(path) then
        vim.opt.dictionary:append(path)
    end
end
dictionary_load_if_exists("/usr/share/dict/words")
dictionary_load_if_exists("/usr/share/thesaurus/moby-thesaurus.txt")
-- }}}

-- spellchecking {{{
vim.opt.isfname:append("@-@")
vim.opt.spelllang = { "en_us", "sr@latin", "sr" }

local spell_root = vim.fn.stdpath("config") .. "/spell"

vim.opt.spellfile = {
    spell_root .. "/en.utf-8.add",
    spell_root .. "/sr@latin.utf-8.add",
    spell_root .. "/sr.utf-8.add",
}

-- Spell checking off by default (can be toggled with F5)
vim.opt.spell = false
-- }}}

-- clipboard {{{
-- default clipboard ops to be with *
vim.opt.clipboard = "unnamed"
-- }}}

-- mouse usage {{{
vim.opt.mouse = "a"
-- hide the mouse cursor while typing
vim.opt.mousehide = true
-- }}}

-- editing behaviors {{{

-- Allow for cursor beyond last character
vim.opt.virtualedit = { "onemore", "block" }

-- Abbrev. of messages (avoids 'hit enter')
vim.opt.shortmess:append({
    f = true,
    i = true,
    l = true,
    m = true,
    n = true,
    r = true,
    x = true,
    o = true,
    O = true,
    t = true,
    T = true,
    s = true,
})

-- Backspace for dummies
vim.opt.backspace = { "indent", "eol", "start" }

-- which commands in normal mode wrap to new line
vim.opt.whichwrap = {
    -- backspace
    b = true,
    -- space
    s = true,
    -- h (left)
    h = true,
    -- l (right)
    l = true,
    -- arrow keys
    ["<"] = true,
    [">"] = true,
    -- insert and replace
    ["["] = true,
    ["]"] = true,
}

-- Match, to be used with %
vim.opt.matchpairs:append("<:>")

vim.opt.textwidth = 79
-- }}}

-- buffer {{{
-- Allow buffer switching without saving
vim.opt.hidden = true
-- }}}

-- diff {{{
-- vim.opt.default diff split to vertical
vim.opt.diffopt = "vertical"
-- }}}

-- grep {{{
-- change grepprg to ripgrep if ripgrep is available
if vim.fn.executable("rg") == 1 then
    vim.o.grepprg = "rg --vimgrep --no-heading --smart-case --hidden "
        .. "--follow -g '!{.git,node_modules}/*'"
end
-- }}}

-- syntax {{{
-- syntax highlighting
vim.fn.syntax = true
-- }}}

-- history, backups, mkview and undo {{{
vim.opt.backup = true
-- only save backups in .local
vim.opt.backupdir = vim.fn.stdpath("state") .. "/backup"
-- So is persistent undo ...
vim.opt.undofile = true
-- Maximum number of changes that can be undone
vim.opt.undolevels = 1000
-- Maximum number lines to save for undo on a buffer reload
vim.opt.undoreload = 10000
-- Store a ton of history (default is 20)
vim.opt.history = 10000
-- Better Unix / Windows compatibility for mkview
vim.opt.viewoptions = { "folds", "options", "cursor", "unix", "slash" }

-- }}}

-- theming {{{

-- colorscheme
-- local colorscheme = require("helpers.colorscheme")
-- vim.cmd.colorscheme(colorscheme)

vim.opt.termguicolors = true
-- terminal colors for internal terminal
vim.g.terminal_color_0 = "#4E4E4E"
vim.g.terminal_color_1 = "#D68787"
vim.g.terminal_color_2 = "#5F865F"
vim.g.terminal_color_3 = "#D8AF5F"
vim.g.terminal_color_4 = "#85ADD4"
vim.g.terminal_color_5 = "#D7AFAF"
vim.g.terminal_color_6 = "#87AFAF"
vim.g.terminal_color_7 = "#D0D0D0"
vim.g.terminal_color_8 = "#626262"
vim.g.terminal_color_9 = "#D75F87"
vim.g.terminal_color_10 = "#87AF87"
vim.g.terminal_color_11 = "#FFD787"
vim.g.terminal_color_12 = "#ACD4FA"
vim.g.terminal_color_13 = "#FFAFAF"
vim.g.terminal_color_14 = "#87D7D7"
vim.g.terminal_color_15 = "#E4E4E4"

-- Assume a dark background
vim.opt.background = "dark"
-- }}}

-- tabs, splits and windows {{{
-- :s[ubstite] preview
vim.opt.inccommand = "split"
-- Only show 15 tabs
vim.opt.tabpagemax = 15
-- Puts new vsplit windows to the right of the current
vim.opt.splitright = true
-- Puts new split windows to the bottom of the current
vim.opt.splitbelow = true
-- }}}

-- cursor {{{
-- Highlight current line
vim.opt.cursorline = true
-- Color this column
vim.opt.colorcolumn = "79"
-- }}}

-- statusline, commandline, ruler {{{

-- Show partial commands in status line and selected characters/lines
-- in visual mode
vim.opt.showcmd = true

-- Display the current mode
vim.opt.showmode = true

-- Show the ruler
vim.opt.ruler = true
-- A ruler on steroids
vim.o.rulerformat = "%30(%=:b%n%y%m%r%w %l,%c%V %P%)"

-- always enable statusline
vim.opt.laststatus = 2

-- Broken down into easily includeable segments

vim.o.statusline = table.concat({
    -- truncate sign
    "%<",
    -- buffer number
    "[%n] ",
    -- Full path
    "%F ",
    -- Preview flag: [Preview]
    "%w",
    -- Help flag: [Help]
    "%h",
    -- Modified flag: [+], [-]
    "%m",
    -- Readonly flag: [RO]
    "%r",
    -- '[Quickfix List]', '[Location List]' or empty.
    "%q",
    -- Type: [vim]
    "%y",
    -- fileformat
    "[%{&ff}]",
    -- Git Hotness
    -- "%{fugitive#statusline()}",
    -- separator
    "%=",
    -- %l - Line; %c - Column, %V - virtual column
    "%-14.(%l,%c%V%) ",
    -- Percentage
    "%P",
})
-- }}}

-- search {{{
-- Case insensitive search
vim.opt.ignorecase = true
-- Case sensitive when uppercase characters present
vim.opt.smartcase = true
-- Show matching brackets/parenthesis
vim.opt.showmatch = true
-- Find as you type search
vim.opt.incsearch = true
-- }}}

-- command completion {{{
-- Case insensitive commands
vim.opt.wildignorecase = true
-- Show list instead of just completing
vim.opt.wildmenu = true
-- Command <Tab> completion: list matches → longest common part → all
vim.opt.wildmode = { "longest", "list", "full" }
-- }}}

-- gutter {{{
-- No extra spaces between rows
vim.opt.linespace = 0
-- Line numbers on
vim.opt.number = true
-- Relative numbers on
vim.opt.relativenumber = true
-- Highlight search terms
vim.opt.hlsearch = true
-- Gutter always there
vim.opt.signcolumn = "yes"
-- }}}

-- windows size and scrolling {{{
-- Windows can be 0 line high
vim.opt.winminheight = 0
-- Lines to scroll when cursor leaves screen
vim.opt.scrolljump = 5
-- Minimum lines to keep above and below cursor
vim.opt.scrolloff = 2
-- }}}

-- folding {{{
-- Auto fold code
vim.opt.foldenable = true
-- Pretty much anything opens a fold vim.opt.list
vim.opt.foldopen = {
    "block",
    "hor",
    "insert",
    "jump",
    "mark",
    "percent",
    "quickfix",
    "search",
    "tag",
    "undo",
}
-- }}}

-- default formatting options {{{
-- Do not wrap long lines
vim.opt.wrap = false
-- Indent at the same level of the previous line
vim.opt.autoindent = true
-- Use indents of 4 spaces
vim.opt.shiftwidth = 4
-- Tabs are spaces, not tabs
vim.opt.expandtab = true
-- An indentation every four columns
vim.opt.tabstop = 4
-- Let backspace delete indent
vim.opt.softtabstop = 4
-- Prevents inserting two spaces after punctuation on a join (J)
vim.opt.joinspaces = false
-- }}}
