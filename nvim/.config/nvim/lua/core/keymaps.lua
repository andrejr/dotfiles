-- Remap the leader
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- Diff get / put shortcuts (for diff merging)
vim.keymap.set('', '<Leader>d1', ':diffget 1<CR>')
vim.keymap.set('', '<Leader>d2', ':diffget 2<CR>')
vim.keymap.set('', '<Leader>d3', ':diffget 3<CR>')
vim.keymap.set('', '<Leader>dg', ':diffget<CR>')
vim.keymap.set('', '<Leader>dp', ':diffput<CR>')
vim.keymap.set('', '<Leader>du', ':diffupdate<CR>')

-- Regular bash command mode shortcuts in command mode
vim.keymap.set('c', '<C-p>', "<Up>")
vim.keymap.set('c', '<C-n>', "<Down>")
vim.keymap.set('c', '<M-b>', "<S-Left>")
vim.keymap.set('c', '<M-f>', "<S-Right>")

-- Regular bash/emacs command mode shortcuts in insert mode
vim.keymap.set('i', '<M-b>', "<S-Left>")
vim.keymap.set('i', '<M-f>', "<S-Right>")

-- Toggle spellcheck
vim.keymap.set('n', '<F5>', ":setlocal spell!<CR>")
vim.keymap.set('i', '<F5>', "<C-\\><C-O>:setlocal spell! spell?<CR>")

-- Esc in normal mode clears search highlighting
vim.keymap.set('n', '<silent>', "<Esc> :noh<CR><Esc>")

-- vim.keymap.set('', '<Leader>y', 'and p variants for unnamedplus copy/pasting')
vim.keymap.set({ "n", "v" }, "<Leader>p", '"+p', { remap = true })
vim.keymap.set({ "n", "v" }, "<Leader>P", '"+P', { remap = true })
vim.keymap.set({ "n", "v" }, "<Leader>y", '"+y', { remap = true })
vim.keymap.set({ "n", "v" }, "<Leader>Y", '"+Y', { remap = true })
vim.keymap.set({ "n", "v" }, "<Leader>d", '"+d', { remap = true })
vim.keymap.set({ "n", "v" }, "<Leader>D", '"+D', { remap = true })
vim.keymap.set("n", "<Leader>r", '"+r', { remap = true })
vim.keymap.set("n", "<Leader>R", '"+R', { remap = true })

-- Tab navigation:
-- The following two lines conflict with moving to top and
-- bottom of the screen
vim.keymap.set('', '<S-H>', 'gT')
vim.keymap.set('', '<S-L>', 'gt')

-- Stupid shift key fixes
vim.cmd([[
        command! -bang -nargs=* -complete=file E e<bang> <args>
        command! -bang -nargs=* -complete=file W w<bang> <args>
        command! -bang -nargs=* -complete=file Wq wq<bang> <args>
        command! -bang -nargs=* -complete=file WQ wq<bang> <args>
        command! -bang Wa wa<bang>
        command! -bang WA wa<bang>
        command! -bang Q q<bang>
        command! -bang QA qa<bang>
        command! -bang Qa qa<bang>
    ]])

vim.keymap.set('c', 'Tabe', "tabe")

-- Yank from the cursor to the end of the line, consistent with C and D.
vim.keymap.set('n', 'Y', "y$")

-- Code folding options
vim.keymap.set('n', '<Leader>F0', ":set foldlevel=0<CR>")
vim.keymap.set('n', '<Leader>F1', ":set foldlevel=1<CR>")
vim.keymap.set('n', '<Leader>F2', ":set foldlevel=2<CR>")
vim.keymap.set('n', '<Leader>F3', ":set foldlevel=3<CR>")
vim.keymap.set('n', '<Leader>F4', ":set foldlevel=4<CR>")
vim.keymap.set('n', '<Leader>F5', ":set foldlevel=5<CR>")
vim.keymap.set('n', '<Leader>F6', ":set foldlevel=6<CR>")
vim.keymap.set('n', '<Leader>F7', ":set foldlevel=7<CR>")
vim.keymap.set('n', '<Leader>F8', ":set foldlevel=8<CR>")
vim.keymap.set('n', '<Leader>F9', ":set foldlevel=9<CR>")


-- -- vim-slash overrides all / bindings
-- if isdirectory(expand(g:plug_home . '/vim-slash-lite'))
--     -- Most prefer to toggle search highlighting rather than clear the
--     -- current search results. To clear search highlighting rather than
--     -- toggle it:
--     -- vim.keymap.set('n', '<silent>', "<Leader>/ :nohlsearch<CR>")
--     vim.keymap.set('n', '<silent>', "<Leader>/ :set invhlsearch<CR>")
-- endif

-- Shortcuts
-- Change Working Directory to that of the current file
vim.keymap.set('c', 'cwd', "lcd %:p:h")

-- Visual shifting (does not exit Visual mode)
vim.keymap.set('v', '<', "<gv")
vim.keymap.set('v', '>', ">gv")

-- Allow using the repeat operator with a visual selection (!)
-- http://stackoverflow.com/a/8064607/127816
vim.keymap.set('v', '.', ":normal .<CR>")

-- For when you forget to sudo.. Really Write the file.
vim.keymap.set('c', 'w!!', "w !sudo tee % >/dev/null")

-- Helpers to edit in tabs/splits from normal mode
vim.keymap.set('c', '%%', "<C-R>=fnameescape(expand('%:h')).'/'<cr>")
vim.keymap.set('c', '%^', "<C-R>=fnameescape(expand('%:t'))<cr>")
vim.keymap.set('n', '<Leader>ew', ":e %%")
vim.keymap.set('n', '<Leader>es', ":sp %%")
vim.keymap.set('n', '<Leader>ev', ":vsp %%")
vim.keymap.set('n', '<Leader>et', ":tabe %%")

-- Adjust viewports to the same size
vim.keymap.set('n', '<Leader>=', "<C-w>=")
-- Maximize current viewport
vim.keymap.set('n', '<Leader>-', "<C-w>_")
-- Increase/decrease width of viewport
vim.keymap.set('n', '<Leader>>', "<C-w>15>")
vim.keymap.set('n', '<Leader><', "<C-w>15<")

-- Easier horizontal scrolling
vim.keymap.set('', 'zl', 'zL')
vim.keymap.set('', 'zh', 'zH')

-- Relative number toggling
vim.keymap.set('n', '<Leader>N', ":setlocal relativenumber!<cr>")
