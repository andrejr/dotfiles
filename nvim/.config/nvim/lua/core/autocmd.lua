-- change working directory to current file's path whenever a file is
-- opened

-- off by default
vim.g.autocd = 0
vim.cmd([[
augroup change_working_dir
    autocmd!
    autocmd bufenter * if g:autocd && bufname('') !~? '^[a-za-z0-9]*://' |
                \ lcd %:p:h | endif
augroup end
]])

-- http://vim.wikia.com/wiki/Highlight_unwanted_spaces
-- highlights trailing whitespace
vim.cmd([[
augroup extra_whitespace
    autocmd!
    autocmd BufNewFile,BufRead,InsertLeave * silent!
                \ match ExtraWhitespace /\s\+$/
    autocmd InsertEnter * silent! match ExtraWhitespace /\s\+\%#\@<!$/
augroup END
]])

-- Unset paste on InsertLeave
vim.cmd([[
augroup no_paste
    autocmd!
    autocmd InsertLeave * silent! vim.opt.nopaste
augroup END
]])

-- Whether or not to automatically strip trailing whitespace
vim.g.keep_trailing_whitespace = 1

vim.cmd([[
augroup strip_trailing_ws
    autocmd!
    autocmd FileType c,cpp,mips,asm,java,go,php,javascript,puppet,python,
                \rust,twig,xml,yml,perl,sql,vim,lex,arduino
                \ let g:keep_trailing_whitespace = 1
    autocmd BufWritePre <buffer>
                \ if (g:keep_trailing_whitespace!=1)
                \ | call s:StripTrailingWhitespace()
                \ | endif
augroup END
]])

-- Instead of reverting the cursor to the last position in the buffer, we
-- set it to the first line when editing a git commit message
vim.cmd([[
augroup gitcommit_goto_l0c0
    autocmd!
    au FileType gitcommit au! BufEnter COMMIT_EDITMSG
                \ call setpos('.', [0, 1, 1, 0])
augroup END
]])
