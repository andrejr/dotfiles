# Andrej's dotfiles
These are my personal (Arch Linux) dotfiles.

I use [GNU Stow](https://www.gnu.org/software/stow/) to manage them and keep them all together - it's simpler than symlinking them by hand / script.

There are 3 shell scripts that call Stow included:
* install.sh calls stow for all subfolders (which symlinks the contents of each into appropriate home folder subdirectories)
* reinstall.sh does the same, just with a -R parameter, which deletes the symlinks then stows dirs again
* uninstall.sh removes all of Stow's symlinks
