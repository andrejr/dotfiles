#!/usr/bin/env python
import i3

# get all outputs and workspaces
outputs = i3.get_outputs()
workspaces = i3.get_workspaces()

# sift out the active workspaces
workspace = list(filter(lambda s: s['focused']==True, workspaces))

output = list(filter(lambda s: s['active']==True, outputs))

active_workspaces = [{'ws': o['current_workspace'], 'o': o['name']} for o in output]

focused_workspace = list(filter(lambda f: f['o']==workspace[0]['output'], active_workspaces))[0]
other_workspace = list(filter(lambda f: f['o']!=workspace[0]['output'], active_workspaces))[0]

# switch to the unfocused workspace and move it to the other output
i3.focus(workspace=other_workspace['ws'])
i3.command('move', 'workspace to output '+ focused_workspace['o'])

# switch to the focused workspace and move it to the other output
# this conveniently returns focus to the 'focused' workspace
i3.focus(workspace=focused_workspace['ws'])
i3.command('move', 'workspace to output '+ other_workspace['o'])
