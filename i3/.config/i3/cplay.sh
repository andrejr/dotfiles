#!/bin/sh
# This command will break if you rename it to
# something containing "cmus".

if ! pgrep cmus ; then
  urxvt -e cmus
else
  cmus-remote -u
fi

case $1 in
    full)
        options="--multidisp"
        ;;
    screen)
        options=""
        ;;
    window)
        options="--focused"
        ;;
    select|*)
        options="--select"
        ;;
esac
