#!/usr/bin/env sh

# Kill picom (will be restarted)
pkill picom

# Restart i3
i3-msg restart
