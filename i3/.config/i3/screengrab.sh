#!/usr/bin/env bash
# -*- coding: utf-8 -*-

set -o pipefail

screenshotdir="$HOME/Screenshots"
imagefile="${screenshotdir}/$(date +%Y%m%d%H%M%S).png"

case $1 in
	full)
		options=""
		;;
	window)
		options="--window \"$(xdotool getactivewindow)\""
		;;
	select | *)
		options="--select"
		;;
esac

mkdir -p "${screenshotdir}"
if maim --quiet $options "${imagefile}"; then
	notify-send --category=transfer.complete \
		--urgency=low --icon=gtk-info \
		"Screen grabbed - $1" "saved to ${imagefile}"
	xclip -selection clipboard -t image/png "${imagefile}"
else
	notify-send --category=transfer.error \
		--urgency=critical --icon=gtk-info \
		"Screen grab error" "$1"
fi
