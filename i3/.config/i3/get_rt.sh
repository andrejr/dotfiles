#!/usr/bin/env bash

if [[ "$(uname -r)" =~ .*realtime.* || "$(uname -r)" =~ .*rt.* ]]; then
    echo 'rt'
else
    echo 'non_rt'
fi
