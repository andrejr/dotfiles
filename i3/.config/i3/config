#######################################################################
#                             i3 settings                             #
#######################################################################
workspace_auto_back_and_forth yes

#######################################################################
#                               Startup                               #
#######################################################################
exec_always --no-startup-id ~/.scripts/dock_screensaver
exec --no-startup-id ~/.scripts/grobi_wrapper.sh
exec_always --no-startup-id \
    XSECURELOCK_BLANK_DPMS_STATE=standby \
    XSECURELOCK_BLANK_TIMEOUT=1 \
    XSECURELOCK_SHOW_DATETIME=1 \
    XSECURELOCK_PASSWORD_PROMPT=time_hex \
    XSECURELOCK_DISCARD_FIRST_KEYPRESS=0 \
    xss-lock \
    --notifier=/usr/lib/xsecurelock/dimmer \
    --transfer-sleep-lock -- \
    ~/.scripts/xsecurelock_inhibit &
exec_always --no-startup-id setxkbmap \
    -layout us,rs,rs \
    -variant 'euro,latinyz,yz' \
    -option 'grp:alt_shift_toggle'

exec --no-startup-id /usr/bin/lxqt-policykit-agent
exec_always --no-startup-id dunst

exec_always --no-startup-id hsetroot -solid "#303030"

include ~/.config/i3/`~/.config/i3/get_rt.sh`.conf

#######################################################################
#                          Cosmetic settings                          #
#######################################################################
font pango:Bloomberg Prop Unicode N, Blobmoji 11

#Screensaver and lockscreen
bindsym Control+mod1+l exec $Locker

#######################################################################
#                            Theme and bar                            #
#######################################################################
new_window normal 1px
# class                 border  backgr. text    indic.  child_border
client.focused          #8cafaf #8cafaf #303030 #8cafaf #8cafaf
client.focused_inactive #875f5f #875f5f #d0d0d0 #875f5f #875f5f
client.unfocused        #4e4e4e #4e4e4e #d0d0d0 #4e4e4e #4e4e4e
client.urgent           #d7afaf #d7afaf #303030 #d7afaf #d7afaf
client.placeholder      #303030 #303030 #b2b2b2 #303030 #303030
client.background       #303030

smart_borders on

popup_during_fullscreen smart
bar {
    status_command i3blocks -c ~/.config/i3/i3blocks.conf
    # status_command i3status -c ~/.config/i3/i3status.conf
    mode dock
    position top
    # height `~/.scripts/scale_by_dpi 22`
    font pango:Bloomberg Fixed Unicode N,Blobmoji 11

    colors {
        background #303030
        statusline #d0d0d0
        separator  #8cafaf
#       class              border  backgr. text
        focused_workspace  #8cafaf #8cafaf #303030
        active_workspace   #875f5f #875f5f #d0d0d0
        inactive_workspace #4e4e4e #4e4e4e #d0d0d0
        urgent_workspace   #d7afaf #d7afaf #303030
    }
}


#######################################################################
#                       Keyboard/mouse bindings                       #
#######################################################################
set $mod Mod4

##########################
#  Programs and actions  #
##########################

# start a terminal
bindsym $mod+Return exec TERMINAL=alacritty i3-sensible-terminal

# kill focused window
bindsym $mod+Shift+q kill

# start rofi launcher
bindsym $mod+d exec "rofi -modi run,drun,combi -combi-modi run,drun -show run"
bindsym $mod+Shift+d exec "rofi -modi run,drun,combi -combi-modi run,drun -show drun"

# start rofi window switcher
bindsym $mod+t exec "rofi -modi window -show window"

# start rofi emoji picker
bindsym $mod+Shift+y exec "rofi -modi emoji -show emoji"

# start unipicker
bindsym $mod+y exec "unipicker --command 'rofi -dmenu' --copy"

# Printscreen
bindsym Print exec ~/.config/i3/screengrab.sh full
bindsym Mod4+Print exec ~/.config/i3/screengrab.sh select
bindsym shift+Print exec ~/.config/i3/screengrab.sh window
bindsym control+Print exec gcolor3

# reload the configuration file
bindsym $mod+Shift+c reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r exec ~/.config/i3/restart.sh

# change brightness with laptop keys
bindsym XF86MonBrightnessUp exec /usr/bin/xbacklight -inc 5
bindsym XF86MonBrightnessDown exec /usr/bin/xbacklight -dec 5

# Pulse Audio controls
bindsym XF86AudioRaiseVolume exec --no-startup-id \
    pactl set-sink-volume @DEFAULT_SINK@ +5% && pkill -RTMIN+1 i3blocks
bindsym XF86AudioLowerVolume exec --no-startup-id \
    pactl set-sink-volume @DEFAULT_SINK@ -5% && pkill -RTMIN+1 i3blocks
bindsym XF86AudioMute exec --no-startup-id \
    pactl set-sink-mute @DEFAULT_SINK@ toggle && pkill -RTMIN+1 i3blocks
bindsym XF86AudioMicMute exec --no-startup-id \
    pactl set-source-mute @DEFAULT_SOURCE@ toggle

# calculator key
bindsym XF86Calculator exec gnome-calculator

# touchpad toggle
bindsym XF86TouchpadToggle exec --no-startup-id python ~/.config/i3/touchpad.py

# homepage key
bindsym XF86HomePage exec --no-startup-id firefox

# player keys
bindsym XF86AudioPlay exec ~/.config/i3/cplay.sh
bindsym XF86AudioNext exec cmus-remote -n
bindsym XF86AudioPrev exec cmus-remote -r

# poweroff
bindsym XF86PowerOff exec systemctl poweroff -i

# sleep
bindsym XF86Sleep exec systemctl suspend

# System menu
set $Locker xset s activate

set $mode_system System (l) lock, (e) logout, (s) suspend, (Shift+h) hibernate, (h) hybrid-sleep, (r) reboot, (Shift+s) shutdown, (a) reboot to realtime kernel
mode "$mode_system" {
    bindsym l exec --no-startup-id $Locker, mode "default"
    bindsym e exec --no-startup-id i3-msg exit, mode "default"
    bindsym s exec --no-startup-id systemctl suspend, mode "default"
    bindsym Shift+h exec --no-startup-id systemctl hibernate, mode "default"
    bindsym r exec --no-startup-id systemctl reboot, mode "default"
    bindsym a exec --no-startup-id systemctl reboot --boot-loader-entry=arch-rt.conf, mode "default"
    bindsym Shift+s exec --no-startup-id systemctl poweroff -i, mode "default"
    bindsym h exec --no-startup-id systemctl hybrid-sleep, mode "default"

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

bindsym $mod+Pause mode "$mode_system"
bindsym $mod+Shift+e mode "$mode_system"

bindsym Mod4+u exec dunstctl close
bindsym Mod4+Shift+u exec dunstctl close-all
bindsym Mod4+i exec dunstctl history-pop
bindsym Mod4+o exec dunstctl context

################
#  Navigation  #
################

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+g split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# border changing
bindsym $mod+q border toggle

# swap workspaces
bindsym $mod+Shift+z exec ~/.config/i3/swap_workspace.py
bindsym $mod+Shift+backslash exec xrandr \
    --output DP-2 --primary --auto \
    --output DP-0 --off \
    --output DP-1 --off \
    --output HDMI-0 --off \
    --output HDMI-1 --off \
    --output HDMI-2 --off
bindsym $mod+Shift+bracketright exec ~/.scripts/grobi_wrapper.sh


# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

# move workspace
bindsym $mod+Shift+b move workspace to output left
bindsym $mod+Shift+n move workspace to output right

# scroll among workspaces
bindsym $mod+b workspace prev
bindsym $mod+n workspace next

# scratchpad
bindsym $mod+shift+p move scratchpad
bindsym $mod+p scratchpad show


# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym j resize grow height 10 px or 10 ppt
        bindsym k resize shrink height 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

####################
#  Mouse bindings  #
####################

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# The middle button over a titlebar kills the window
bindsym button2 kill

# The middle button and a modifer over any part of the window kills the window
bindsym $mod+button2 kill

# The right button toggles floating
bindsym button3 floating toggle
bindsym $mod+button3 floating toggle

# The side buttons move the window around
bindsym button9 move left
bindsym button8 move right

###############################################################################
#                                Window rules                                 #
###############################################################################

# use xprop to find out classes
# $ xprop
# WM_WINDOW_ROLE(STRING) = "gimp-toolbox-color-dialog"
# WM_CLASS(STRING) = "gimp-2.8", "Gimp-2.8"
# _NET_WM_NAME(UTF8_STRING) = "Change Foreground Color"
#
# The first part of WM_CLASS is the "instance" (gimp-2.8 in this case), the
# second part is the "class" (Gimp-2.8 in this case). "title" matches against
# _NET_WM_NAME and "window_role" matches against WM_WINDOW_ROLE.
#
#   WM_CLASS           ↔ instance
#   _NET_WM_NAME       ↔ title
#   WM_WINDOW_ROLE     ↔ window_role

###########
#  class  #
###########
for_window [class="Gnome-calculator"] border pixel 1, floating enable
for_window [class="[Cc]hromium"] border pixel 1
for_window [class="Firefox"] border pixel 1
for_window [class="floater"] floating enable
for_window [class="Lxappearance"] floating enable
for_window [class="Sms.py"] floating enable
for_window [class="FreeCAD" title="Formula editor"] border normal, floating enable
for_window [class="^TeamViewer$" title="^TeamViewer$"] floating enable
for_window [class="^Gcolor3$"] floating enable

###########
#  title  #
###########
# for_window [title="Question"] floating enable
for_window [title="Properties"] floating enable

##########
#  role  #
##########
for_window [window_role="pop-up"] floating enable
for_window [window_role="popup"] floating enable

#######################################################################
#                             i3 settings                             #
#######################################################################
workspace_auto_back_and_forth yes
